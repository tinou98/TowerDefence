#include "loaderownfile.h"

SETUP_LOG(LoaderOwnFile, "LOADER-OWN-FILE")

#include <fstream>
#include <iostream>

#include <glm/gtc/type_ptr.hpp>
#include <utils/fps.h>

LoaderOwnFile::LoaderOwnFile(std::string file) : vABo("shaders/default.vert", "shaders/default.frag"), objectIdTable(), indices(GL_ELEMENT_ARRAY_BUFFER), renderListVBO(GL_DRAW_INDIRECT_BUFFER), SSBOPerMesh(0), SSBOPerObject(1) {
	//this->objectIdTable.constructFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_DYNAMIC_STORAGE_BIT;

	this->SSBOPerObject.constructFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT;
	this->SSBOPerObject.mapFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT;

	this->objectIdTable.constructFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT;
	this->objectIdTable.mapFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT;

	/* Open file */ {
		this->pFile = fopen(file.c_str(), "rb");
		setupThrow(LOG, this->pFile != 0, SSTR("Open file : " << file));
	}

	/* Read HEADER */ {
		char h[2];
		fread(h, sizeof(char), 2, this->pFile);
		setupThrow(LOG, h[0] == 'T' && h[1] == 'D', "Openning GOOD file");
	}

	/* Read offsets and sizes */ {
		fread(&this->osObject, sizeof(offsetSize), 1, this->pFile);
		fread(&this->osVbo, sizeof(offsetSize), 1, this->pFile);
		fread(&this->osIdx, sizeof(offsetSize), 1, this->pFile);
		fread(&this->osTextures, sizeof(offsetSize), 1, this->pFile);
	}

	this->debug();

	this->loadInMem();
}

LoaderOwnFile::~LoaderOwnFile() {
	fclose(this->pFile);
}

void LoaderOwnFile::loadInMem() {
	if(this->inMem)
		return;

	LOG.info("Start load in memory");

	this->vABo.shader();
	this->vABo.vao();

	/* Read object data */ {
//		fseek(this->pFile, this->osObject.offset, SEEK_SET);

		std::string name;
		std::uint8_t stringSize;

		for(std::uint32_t i = 0; i < this->osObject.size; ++i) {
			fread(&stringSize, sizeof(stringSize), 1, this->pFile);
			name.resize(stringSize);
			fread(&name[0], sizeof(char), stringSize, this->pFile);

			fread(&this->object[name].firstId, sizeof(this->object[name].firstId), 1, this->pFile);
			fread(&this->object[name].lastId, sizeof(this->object[name].lastId), 1, this->pFile);

			fread(&this->object[name].textureId, sizeof(std::uint8_t), 4, this->pFile);

			/* Read bones data */ {
				std::uint8_t nbBones;
				fread(&nbBones, sizeof(nbBones), 1, this->pFile);

				std::vector<std::uint8_t> parent;
				parent.resize(nbBones);
				fread(parent.data(), sizeof(std::uint8_t), nbBones, this->pFile);

				this->object[name].boneHandler = new BoneHandler(parent);
			}

			/* Read poses */ {
				std::uint8_t nbPoses;
				fread(&nbPoses, sizeof(nbPoses), 1, this->pFile);

				std::vector<PoseData> poseList;
				poseList.resize(nbPoses);

				for(int i = 0; i < nbPoses; i++) {
					std::uint8_t poseNameLength;
					fread(&poseNameLength, sizeof(poseNameLength), 1, this->pFile);
					if(poseNameLength != 0) {
						poseList.at(i).name.resize(poseNameLength);
						fread(&poseList.at(i).name[0], sizeof(char), poseNameLength, this->pFile);
					} else {
						poseList.at(i).name = std::string();
					}

					std::uint8_t nbConstraint;
					fread(&nbConstraint, sizeof(nbConstraint), 1, this->pFile);

					std::vector<Constraint> constraintList;
					poseList.at(i).constraintList.resize(nbConstraint);
					for(int j = 0; j < nbConstraint; j++) {
						fread(&poseList.at(i).constraintList.at(j).boneId, sizeof(std::uint8_t), 1, this->pFile);

						vec3 pos;
						vec4 quaternion;
						vec3 scale; // BAD
#warning NO SCALE (pls)
						fread(&pos, sizeof(pos), 1, this->pFile);
						fread(&quaternion, sizeof(quaternion), 1, this->pFile);
						fread(&scale, sizeof(scale), 1, this->pFile);

						poseList.at(i).constraintList.at(j).constraint.setOrigin(btVector3(pos.x, pos.y, pos.z));
						poseList.at(i).constraintList.at(j).constraint.setRotation(btQuaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w));
					}
				}

				this->object[name].poseHandler = new PoseHandler(poseList);
			}

			/* Read Animation */ {
				std::uint8_t nbAnimation;
				fread(&nbAnimation, sizeof(nbAnimation), 1, this->pFile);

				for(int i = 0; i < nbAnimation; i++) {
					std::uint8_t animNameLength;
					std::string name;
					fread(&animNameLength, sizeof(animNameLength), 1, this->pFile);
					if(animNameLength != 0) {
						name.resize(animNameLength);
						fread(&name[0], sizeof(char), animNameLength, this->pFile);
					}

					float duration;
					std::uint8_t keyframeNb;
					fread(&duration, sizeof(duration), 1, this->pFile);
					fread(&keyframeNb, sizeof(keyframeNb), 1, this->pFile);
					for(int kf = 0; kf < keyframeNb; kf++) {
						float currTime;
						std::uint8_t poseId;

						fread(&currTime, sizeof(currTime), 1, this->pFile);
						fread(&poseId, sizeof(poseId), 1, this->pFile);
					}
				}
			}

			this->object[name].drawCmd = i;
			LOG.info(SSTR(name << " (" << i << ")\t" << this->object[name].firstId << "-" << this->object[name].lastId));
		}
	}

	/* Read PixelData */ {
//		fseek(this->pFile, this->offsetVBO, SEEK_SET);
		PixelData *tmpData = new PixelData[this->osVbo.size];
		fread(tmpData, sizeof(PixelData), this->osVbo.size, this->pFile);

		this->vABo.vbo.initStatic(this->osVbo.size * sizeof(PixelData), 0, tmpData);

		// Pos
		vboConfig(0, 3, GL_FLOAT, GL_FALSE, sizeof(PixelData), offsetof(PixelData, pos))();

		// UV
		vboConfig(1, 2, GL_FLOAT, GL_FALSE, sizeof(PixelData), offsetof(PixelData, uv))();

		// Bones idx
		vboConfig(2, 4, GL_UNSIGNED_INT, GL_FALSE, sizeof(PixelData), offsetof(PixelData, bonesId), 0, vboConfig::internalType::Int)();
		// Bones Weight
		vboConfig(3, 4, GL_FLOAT, GL_FALSE, sizeof(PixelData), offsetof(PixelData, bonesWeight))();

		delete[] tmpData;
	}

	/* Read Indices */ {
//		fseek(this->pFile, this->offsetIdx, SEEK_SET);
		GLushort tmpIdx[this->osIdx.size];
		fread(tmpIdx, sizeof(GLushort), this->osIdx.size, this->pFile);

		this->indices.initStatic(this->osIdx.size * sizeof(GLushort), 0, tmpIdx);
	}

	/* Render list */ {
		DrawElementsIndirectCommand commandList[this->object.size()];

		unsigned int sum = 0;
		for(auto& e : this->object) {
			commandList[e.second.drawCmd].count = e.second.lastId - e.second.firstId + 1;
			commandList[e.second.drawCmd].instanceCount = 0;
			commandList[e.second.drawCmd].firstIndex = e.second.firstId;
			commandList[e.second.drawCmd].baseVertex = 0;
			commandList[e.second.drawCmd].baseInstance = sum;

			sum += commandList[e.second.drawCmd].instanceCount;
		}

		this->renderListVBO.initStatic(this->object.size() * sizeof(DrawElementsIndirectCommand), GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT, commandList);
		this->renderListVBO();

		this->drawIndirectCmd = static_cast<DrawElementsIndirectCommand *>(this->renderListVBO.map(0, this->object.size() * sizeof(DrawElementsIndirectCommand), GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_FLUSH_EXPLICIT_BIT));
	}

	/* Object ID */ {
		this->objectIdTable.setItems({vboConfig(4, 1, GL_UNSIGNED_SHORT, GL_FALSE, 0, 0, 1, vboConfig::internalType::Int)});
		this->objectIdTable();
	}

	/* Per Mesh */ {
#warning sizeof(0) <-> To change
		this->SSBOPerMesh.initStatic(this->object.size() * sizeof(0), 0, nullptr);
		this->SSBOPerMesh();
	}

	/* Per Object */ {
		this->SSBOPerObject();
		this->SSBOPerObject.map();
	}

	/* Load Texture array */ {
//		fseek(this->pFile, this->offsetTex, SEEK_SET);
//		this->imgArr.loadCustomFile(this->pFile);
	}

	VAO::clear();

	this->inMem = true;
}

void LoaderOwnFile::render(glm::mat4 m) {
	this->vABo();
//	this->imgArr();


	for(EntityProxy* obj : this->children) {
		obj->update(1.0/60.0);
	}

	this->SSBOPerObject.map();
	this->SSBOPerObject.flushMap();

	Pipeline::uniformLoc mvpLoc = this->vABo.shader.getUniformLocation("VP");
	glProgramUniformMatrix4fv(mvpLoc.program, mvpLoc.uniformId, 1, GL_FALSE, glm::value_ptr(m));

	this->SSBOPerObject();
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_SHORT, nullptr, this->object.size(), 0);
}

#include <physics/collisionshape.h>
EntityProxy* LoaderOwnFile::createMesh(std::string name) {
	Object obj;
	try {
		obj = this->object.at(name);
	} catch(...) {
		LOG.fail(SSTR("Unable to find mesh " << name));
		return nullptr;
	}

	unsigned int offset = obj.drawCmd;

	std::uint16_t id = 0;
	/* Get new ID */ {

//		if(!this->freeIdx.freePlace.empty()) {
#warning Not Working
//			id = this->freeIdx.freePlace.front();
//			this->freeIdx.freePlace.pop_back();

//			this->SSBOPerObject.set(id, d);
//		} else {
			id = this->freeIdx.lastIdx;
			this->freeIdx.lastIdx++;

			// As we take a new space, increase SSBO size
			this->SSBOPerObject.incSize(obj.boneHandler->getNumberBones());
//		}
	}

	std::uint16_t realId = 0;
	/* Update Draw Indirect Command */ {
		realId = this->drawIndirectCmd[offset].baseInstance + this->drawIndirectCmd[offset].instanceCount;

		// Increment instanceCount and all next baseInstance
		this->drawIndirectCmd[offset].instanceCount++;
		for(int i = offset+1; i < this->object.size(); i++)
			this->drawIndirectCmd[offset].baseInstance++;

		auto off = offset * sizeof(DrawElementsIndirectCommand);
		this->renderListVBO.flushMap(off, this->object.size() * sizeof(DrawElementsIndirectCommand) - off);
	}

	/* Shift IdTable array */ {
		// Add a new element
		this->objectIdTable.pushBack(0);

		GLushort *idTbl = *this->objectIdTable.getMappedPtr();

		for(int i = this->objectIdTable.getSize()-2; i >= realId; i--)
			idTbl[i+1] = idTbl[i];

		idTbl[realId] = id;


		this->objectIdTable.flushMap();
	}

#warning To change {
	Physics::ColisionShapeParam p = {};
	p.BoxShape.boxHalfExtents = btVector3(1, 1, 1);
	Physics::CollisionShape *shape = new Physics::CollisionShape(Physics::CollisionType::BoxShape, p);
	shape->mass = 1.0;
	EntityProxy *ret = new EntityProxy(this, id, *shape, obj);
	this->children.push_back(ret);
	return ret;
#warning }
}

void LoaderOwnFile::removeMesh(EntityProxy *item) {
	this->freeIdx.freePlace.push_back(item->id);
#warning Reindex all stuff
}

void LoaderOwnFile::debug() {
	LOG.info(SSTR(std::endl << "===== FILE read DATA =====" << std::endl
				  << "Taille VBO :" << this->osVbo.size << std::endl
				  << "Nombre d'Index :" << this->osIdx.size << std::endl
				  << "Nombre de mesh :" << this->osObject.size << std::endl
				  << std::endl << "===== LIST of BUFFER =====" << std::endl
				  << "VAO " << GLuint(this->vABo.vao) << std::endl
				  << "VBO " << GLuint(this->vABo.vbo) << std::endl
				  << "indices " << GLuint(this->indices) << std::endl
				  << "renderListVBO " << GLuint(this->renderListVBO) << std::endl
				  << "objectIdTable " << GLuint(this->objectIdTable) << std::endl
				  << "SSBOPerMesh " << GLuint(this->SSBOPerMesh) << std::endl
				  << "SSBOPerObject " << GLuint(this->SSBOPerObject) << std::endl
				  << "=========================="));
}
