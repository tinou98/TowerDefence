#ifndef IMAGE_H
#define IMAGE_H

#include <nvidia/nv_dds.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <utils/log.h>

class Image {
public:
	Image();
	Image(const std::string &fileName, bool flipImage = true);
	bool load(const std::string &fileName, bool flipImage = true);

	void operator ()();
	operator GLuint() { return this->txtId; }

private:
	ADD_DEFAULT_LOG

protected:
	GLuint txtId;
};

#endif // IMAGE_H
