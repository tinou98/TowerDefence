#include "bonehandler.h"

BoneHandler::BoneHandler(std::vector<std::uint8_t> parent) {
	int nbBones = parent.size();

	this->nodeList.reserve(nbBones);
	for(int i = 0; i < nbBones; i++)
		this->nodeList.push_back(new Bone(i, parent.at(i)));

	for(int i = 0; i < nbBones; i++) {
		if(parent.at(i) == i)
			this->rootNodes.push_back(this->nodeList[i]);
		else
			this->nodeList[parent[i]]->addChildren(this->nodeList[i]);
	}
}

BoneHandler::BoneHandler(const BoneHandler &source) {
	int nbBones = source.getNumberBones();

	this->nodeList.reserve(nbBones);
	for(int i = 0; i < nbBones; i++)
		this->nodeList.push_back(new Bone(i, source.nodeList.at(i)->parentId));

	for(int i = 0; i < nbBones; i++) {
		if(source.nodeList.at(i)->parentId == i)
			this->rootNodes.push_back(this->nodeList.at(i));
		else
			this->nodeList.at(source.nodeList.at(i)->parentId)->addChildren(this->nodeList.at(i));
	}
}

BoneHandler::~BoneHandler() {
	for(const auto &list : this->nodeList)
		delete list;
}

std::int8_t BoneHandler::getNumberBones() const {
	return this->nodeList.size();
}

Physics::MotionState *BoneHandler::getMotionState(int idx) {
	return this->nodeList.at(idx)->motionState;
}

void BoneHandler::setMotionState(int idx, Physics::MotionState *motionState) {
	this->nodeList.at(idx)->motionState = motionState;
}

void BoneHandler::update(btTransform origin, float deltaTime) {
	for(Bone *node : this->rootNodes)
		node->update(origin, deltaTime);
}

void BoneHandler::applyTo(btTransform pos, std::uint8_t id, float delay) {
	this->nodeList.at(id)->apply(pos, delay	);
}
