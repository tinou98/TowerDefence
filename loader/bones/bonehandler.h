#ifndef BONEHANDLER_H
#define BONEHANDLER_H

#include <cstdint>
#include <list>
#include <vector>

#include "bone.h"

class BoneHandler {
public:
	BoneHandler(std::vector<std::uint8_t> parent);
	BoneHandler(const BoneHandler &source);
	~BoneHandler();

	std::int8_t getNumberBones() const;

	void applyTo(btTransform pos, std::uint8_t id, float delay = 0);

	Physics::MotionState *getMotionState(int idx);
	void setMotionState(int idx, Physics::MotionState *motionState);

	void update(btTransform origin, float deltaTime);

private:
	std::vector<Bone*> nodeList;
	std::list<Bone*> rootNodes;
};

#endif // BONEHANDLER_H
