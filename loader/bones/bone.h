#ifndef BONE_H
#define BONE_H

#include <cstdint>
#include <LinearMath/btTransform.h>
#include <forward_list>

#include <physics/motionstate.h>

class BoneHandler;

class Bone {
	friend BoneHandler;
public:
	Bone(std::uint8_t id,std::uint8_t parentId, Physics::MotionState *motionState = nullptr);
	~Bone();

	void addChildren(Bone *child);
	Physics::MotionState *motionState;

	void apply(btTransform pos, float duration = 0);

private:
	void update(btTransform parentPos, float deltaTime);
	btTransform getCurrentPos() const;

	std::uint8_t id;
	std::uint8_t parentId;

	/* Animation data */
	btTransform startPos;
	btTransform targetPos;
	float currTime;
	float duration;

	btTransform globalParentPos;


	std::forward_list<Bone*> childrens;
};

#endif // BONE_H
