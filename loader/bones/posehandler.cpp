#include "posehandler.h"

#include <utils/log.h>

PoseHandler::PoseHandler(const std::vector<PoseData> &poseList) {
	this->poseList.reserve(poseList.size());
	for(const PoseData &pose : poseList) {
		// Insert link to name
		if(!pose.name.empty())
			this->poseByName.insert({pose.name, this->poseList.size()});

		this->poseList.push_back(new Pose(pose.constraintList));
	}
}

PoseHandler::~PoseHandler() {
	for(Pose *pose : this->poseList)
		delete pose;
}

void PoseHandler::setPose(BoneHandler *boneHandler, std::string poseName, float delay) {
	try {
		this->setPose(boneHandler, this->poseByName.at(poseName), delay);
	} catch(const std::out_of_range &) {
		throw std::out_of_range(SSTR("Try to set pose (with name " << poseName << ") that doesn't exist"));
	}
}

#include <iostream>
void PoseHandler::setPose(BoneHandler *boneHandler, std::uint8_t poseId, float delay) {
	std::cout << "Set pose : " << this << "\t" << boneHandler << "\t" << (int)poseId << "\t" <<  delay << std::endl
			  << this->poseList.at(poseId) << std::endl;
	this->poseList.at(poseId)->applyTo(boneHandler, delay);
}
