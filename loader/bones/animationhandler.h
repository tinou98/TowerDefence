#ifndef ANIMATIONHANDLER_H
#define ANIMATIONHANDLER_H

#include <cstdint>
#include <vector>
#include <map>
#include <string>

#include "animation.h"
#include "bonehandler.h"

class AnimationHandler {
public:
	AnimationHandler();
	~AnimationHandler();

	void startAnimation(BoneHandler *boneHandler, std::string animName, float delay = 0);
	void startAnimation(BoneHandler *boneHandler, std::uint8_t animId, float delay = 0);

private:
	std::vector<Animation*> animList;
	std::map<std::string, std::int8_t> animByName;
};

#endif // ANIMATIONHANDLER_H
