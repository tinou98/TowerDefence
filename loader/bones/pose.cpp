#include "pose.h"

Pose::Pose(const std::vector<Constraint> &cList) : contrainstList(cList) {}

#include <iostream>
void Pose::applyTo(BoneHandler *boneHandler, float delay) {
	std::cout << "applyTo " << this << "\t" << boneHandler << "\t" << delay << std::endl
			  << &this->contrainstList << std::endl;
	for(const Constraint &c : this->contrainstList) {
		std::cout << "constraint " << c.boneId << "\t" << &c.constraint << std::endl;
		boneHandler->applyTo(c.constraint, c.boneId, delay);
	}
}
