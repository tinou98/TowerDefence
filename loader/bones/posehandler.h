#ifndef POSEHANDLER_H
#define POSEHANDLER_H

#include <cstdint>
#include <vector>
#include <map>
#include <string>

#include "pose.h"
#include "bonehandler.h"

struct PoseData {
	std::string name;
	std::vector<Constraint> constraintList;
};

class PoseHandler {
public:
	PoseHandler(const std::vector<PoseData> &poseList);
	~PoseHandler();

	void setPose(BoneHandler *boneHandler, std::string poseName, float delay = 0);
	void setPose(BoneHandler *boneHandler, std::uint8_t poseId, float delay = 0);

private:
	std::vector<Pose*> poseList;
	std::map<std::string, std::int8_t> poseByName;
};

#endif // POSEHANDLER_H
