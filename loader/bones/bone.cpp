#include "bone.h"

#include <cmath>

Bone::Bone(std::uint8_t id, std::uint8_t parentId, Physics::MotionState *motionState) : id(id), parentId(parentId), motionState(motionState) {
	this->startPos.setIdentity();
	this->globalParentPos.setIdentity();

	this->duration = 0;
}

Bone::~Bone() {
	if(this->motionState != nullptr)
		delete this->motionState;
}

void Bone::addChildren(Bone *child) {
	if(this == child)
		return;

	this->childrens.push_front(child);
}

void Bone::apply(btTransform pos, float duration) {
	this->currTime = 0;
	this->duration = duration;

	this->startPos = getCurrentPos();
	this->targetPos = pos;
}

btTransform Bone::getCurrentPos() const {
	float percentage = this->currTime;
	return btTransform(
				this->startPos.getRotation().slerp(this->targetPos.getRotation(), percentage),
				this->startPos.getOrigin().lerp(this->targetPos.getOrigin(), percentage)
	);
}

void Bone::update(btTransform parentPos, float deltaTime) {
	btTransform globalPos(parentPos * this->getCurrentPos());

	if(this->duration != 0)
		this->currTime += deltaTime;

	/* Animation is over */
	if(this->currTime >= this->duration)
			this->duration = 0;

	for(Bone *&bone: this->childrens)
		bone->update(globalPos, deltaTime);

	if(this->motionState != nullptr)
		this->motionState->setWorldTransform(globalPos);
}
