#ifndef POSE_H
#define POSE_H

#include <list>
#include <cstdint>
#include <LinearMath/btTransform.h>
#include "bonehandler.h"

struct Constraint {
	std::uint8_t boneId;
	btTransform constraint;
};

class Pose {
public:
	Pose(const std::vector<Constraint> &cList);

	void applyTo(BoneHandler *boneHandler, float delay = 0);

private:
	const std::vector<Constraint> contrainstList;
};

#endif // POSE_H
