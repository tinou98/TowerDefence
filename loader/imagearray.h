#ifndef IMAGEARRAY_H
#define IMAGEARRAY_H

#include "image.h"

#include <list>
#include <utility>

class ImageArray : public Image {
public:
	ImageArray();
	ImageArray(std::list<std::string> fileName, bool flipImage);
	int add(std::string, bool flipImage);

	void loadCustomFile(std::string fileName);
	void loadCustomFile(FILE *pFile);

	void load();
	void operator ()();

private:
	ADD_DEFAULT_LOG

	unsigned int height = 0,
				 width = 0,
				 mipMapNb = 0,
				 internalFormat = 0,
				 maxSize = 0;

	std::list <std::pair<std::string, bool>> imageList;
};

#endif // IMAGEARRAY_H
