#include "entityproxy.h"

#include "loaderownfile.h"

EntityProxy::EntityProxy(LoaderOwnFile *parent, unsigned int id, const Physics::CollisionShape &shape, const Object obj) : Entity3D(btTransform::getIdentity(), shape), parent(parent), id(id) {
	/*this->state.updateCallback = [this](glm::mat4 mat) {
		this->updatePosition(mat);
	};*/

	this->boneHandler = new BoneHandler(*obj.boneHandler);
	this->poseHandler = obj.poseHandler;

	/* Setup MotionState */
	for(int i = 0; i < this->boneHandler->getNumberBones(); i++) {
		Physics::MotionState *motionState = new Physics::MotionState(btTransform::getIdentity());

		motionState->updateCallback = [this, i](glm::mat4 mat) {
			auto currId = this->id + i;
			auto pos = this->parent->SSBOPerObject[currId];
			memcpy(pos.mat, glm::value_ptr(mat), 4*4*4);
			this->parent->SSBOPerObject.set(currId, pos);
		};

		this->boneHandler->setMotionState(i, motionState);
	}
}

EntityProxy::~EntityProxy() {
	this->parent->removeMesh(this);
}

void EntityProxy::update(float deltaTime) {
	btTransform origin;
	this->state.getWorldTransform(origin);

	this->boneHandler->update(origin, deltaTime);
}

void EntityProxy::updatePosition(glm::mat4 mat) {
	auto pos = this->parent->SSBOPerObject[this->id];
	memcpy(pos.mat, glm::value_ptr(mat), 4*4*4);
	this->parent->SSBOPerObject.set(this->id, pos);
}
