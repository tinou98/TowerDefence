#ifndef MTLLOADER_H
#define MTLLOADER_H

#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

#include <utils/log.h>
#include "image.h"

struct MTL {
	glm::vec3 Kd;
	std::string mapKd;
};

class MtlLoader {
public:
	MtlLoader();
	~MtlLoader();

	bool load(std::string filePath);

	bool lockItem(std::string s);
	std::string itemLocked() {
		if(this->it == this->lst.end())
			return "";

		return this->it->first;
	}

	MTL getMaterial();
	glm::vec3 getColor();
	Image* getImage();

private:
	static const MTL def;
	ADD_DEFAULT_LOG

	std::unordered_map<std::string, MTL> lst;
	std::unordered_map<std::string, MTL>::const_iterator it;


	std::unordered_map<std::string, Image*> imgBuffer;
};

#endif // MTLLOADER_H
