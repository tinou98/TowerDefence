#include "image.h"

#include <utils/fps.h>

SETUP_LOG(Image, "IMAGE")

#include <fstream>
Image::Image() {
	glGenTextures(1, &this->txtId);
}

Image::Image(const std::string &fileName, bool flipImage) : Image() {
	load(fileName, flipImage);
}

bool Image::load(const std::string &fileName, bool flipImage) {
	Fps f;

//	fileName = fileName.substr(0, fileName.find_last_of(".")) + ".dds";

	glGenTextures(1, &this->txtId);

	FILE *file = fopen(fileName.c_str(), "r");
	setupNoRet(LOG, file, SSTR("Loading " << fileName), return false;);

	dds::CDDSImage img;
	img.load(file, flipImage);
	fclose(file);

	glBindTexture(GL_TEXTURE_2D, this->txtId); {
		glCompressedTexImage2D(GL_TEXTURE_2D, 0, img.get_format(), img.get_width(), img.get_height(), 0, img.get_size(), img);

//		for(unsigned int i = 0; i < img.get_num_mipmaps(); i++) {
//			dds::CSurface mipmap = img.get_mipmap(i);

//			glCompressedTexImage2D(GL_TEXTURE_2D, i+1, img.get_format(), mipmap.get_width(), mipmap.get_height(), 0, mipmap.get_size(), mipmap);
//		}

//		if(img.get_num_mipmaps() < 1) {
			LOG.info("Generate MIPMAP");
			glGenerateMipmap(GL_TEXTURE_2D);
//		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	} glBindTexture(GL_TEXTURE_2D, 0);

	LOG.done(SSTR("Loading in " << 1000.0*f.end() << "ms"));
}

#include <iostream>
void Image::operator ()() {
	glBindTexture(GL_TEXTURE_2D, this->txtId);
}
