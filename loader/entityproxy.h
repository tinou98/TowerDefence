#ifndef ENTITYPROXY_H
#define ENTITYPROXY_H

#include <entity/3D/entity3d.h>

#include "bones/bonehandler.h"
#include "bones/posehandler.h"
struct Object;

class LoaderOwnFile;

class EntityProxy : public Entity3D {
	friend class LoaderOwnFile;

public:

	BoneHandler *boneHandler;
	PoseHandler *poseHandler;
private:
	EntityProxy(LoaderOwnFile *parent, unsigned int id, const Physics::CollisionShape &shape, const struct Object obj);
	~EntityProxy();

	void update(float deltaTime);

	unsigned int id;
	LoaderOwnFile *parent;


protected:
	void updatePosition(glm::mat4 mat);
};

#endif // ENTITYPROXY_H
