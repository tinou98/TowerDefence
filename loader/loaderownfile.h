#ifndef LOADEROWNFILE_H
#define LOADEROWNFILE_H

#include <utils/log.h>
#include <utils/glmutils.h>
#include <utils/buffer/buffer.h>
#include <utils/buffer/vbo.h>
#include <utils/buffer/vectorgpu.h>
#include <utils/buffer/ssbo.h>
#include <entity/3D/entity3d.h>
#include <entity/3D/loader.h>
#include <loader/imagearray.h>
#include <loader/entityproxy.h>
#include <loader/bones/bonehandler.h>
#include <loader/bones/posehandler.h>
#include <loader/bones/animationhandler.h>
#include <string>
#include <vector>
#include <unordered_map>

struct PixelData {
	vec3 pos;
	vec2 uv;
	vec4 bonesId;
	vec4 bonesWeight;
};

struct DrawElementsIndirectCommand {
	GLuint count;
	GLuint instanceCount;
	GLuint firstIndex;
	GLuint baseVertex;
	GLuint baseInstance;
};

struct Object {
	std::uint16_t firstId,
			lastId;

	std::uint8_t textureId[4];

	BoneHandler *boneHandler;			// Will be copied on each instance
	PoseHandler *poseHandler;			// List of all pose : use setPose(boneHandler, poseId, duration = 0)
	AnimationHandler *animHandler;		// List of all animation : use executeAnim(animId, duration = 0)

	unsigned int drawCmd = 0;
};

struct ItemData {
	float mat[4*4];
};

class LoaderOwnFile /*: public Entity3D*/ {
	friend class EntityProxy;
public:
	explicit LoaderOwnFile(std::string file);
	~LoaderOwnFile();

	EntityProxy* createMesh(std::string name);
	void removeMesh(EntityProxy *item);

	void loadInMem();
	void unloadFromMem();

	void render(glm::mat4 m);

	void debug();

//private:
	ADD_DEFAULT_LOG

	bool inMem = false;

	struct __attribute__ ((packed)) offsetSize {
		std::uint64_t offset;							/*<! Absolute offset */
		std::uint32_t size;								/*<! Size (in number of block) */
	} osObject, osVbo, osIdx, osTextures;

	std::unordered_map<std::string, Object> object;

	std::vector<EntityProxy*> children;

	// GPU Data
	VectorGPU<GLushort, VBO> objectIdTable;				/*<! Contain the ID of objects (Dynamic resize) Heavy change -> must be maped */
	Buffer renderListVBO,								/*<! RenderList (Dynamic, but no resize) */
			indices;									/*<! Draw Indices (Static) */
	SSBO SSBOPerMesh;									/*<! Contain Per Mesh Data (Static) */

	VectorGPU<ItemData, SSBO> SSBOPerObject;	/*<! Contain Per Object Data (Dynamic resize) Use first free, reindex objectIdTable, work on chunk, persistant map may be good */

	DrawElementsIndirectCommand *drawIndirectCmd;

	struct {
		std::vector<std::uint16_t> freePlace;
		std::uint16_t lastIdx = 0;
	} freeIdx;



//	ImageArray imgArr;

	VertexArrayBufferObject vABo;

	FILE* pFile;
};

inline bool operator==(const PixelData& lhs, const PixelData& rhs) {
	return (
				(lhs.pos == rhs.pos) &&
				(lhs.uv == rhs.uv)
			);
}

inline bool operator!=(const PixelData& lhs, const PixelData& rhs) {
	return !(lhs == rhs);
}

#endif // LOADEROWNFILE_H
