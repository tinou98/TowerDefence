#include "imagearray.h"
#include <nvidia/nv_dds.h>

#include "loaderownfile.h"

SETUP_LOG(ImageArray, "IMAGE-ARRAY")

ImageArray::ImageArray() : Image() {}

ImageArray::ImageArray(std::list<std::string> fileName, bool flipImage) : ImageArray() {
	for(auto file : fileName)
		this->add(file, flipImage);
}

int ImageArray::add(std::string fileName, bool flipImage) {
	dds::CDDSImage img;

	if(!img.load(fileName, flipImage)) {
		LOG.fail(SSTR("Load file " << fileName));
		return -1;
	}

	if(this->height == 0 && this->width == 0 && this->internalFormat == 0) {
		this->height			= img.get_height();
		this->width				= img.get_width();

		this->internalFormat	= img.get_internal_format();

		LOG.done(SSTR("Load file as refence\t" << fileName << "\t" << this->height << "x" << this->width));

	} else {
		setupThrow(LOG, (this->height == img.get_height() && this->width == img.get_width() && this->internalFormat == img.get_internal_format()),
				   SSTR("Test size and format\t" << fileName))
	}

	if(this->mipMapNb < img.get_num_mipmaps()) {
		this->mipMapNb = img.get_num_mipmaps();
		this->maxSize = img.get_size();
	}

	this->imageList.push_back({fileName, flipImage});

	return this->imageList.size() - 1;
}

void ImageArray::loadCustomFile(std::string file) {
	FILE *pFile;

	/* Open file */ {
		pFile = fopen(file.c_str(), "rb");
		setupThrow(LOG, pFile != 0, SSTR("Open file : " << file));
	}

	/* Read HEADER */ {
		char h[2];
		fread(h, sizeof(char), 2, pFile);
		setupThrow(LOG, h[0] == 'T' && h[1] == 'D', "Openning GOOD file");
	}

	/* Read VBO & Idx size */ {
		std::uint32_t sizeVBO, sizeIdx;

		fread(&sizeVBO, sizeof(sizeVBO), 1, pFile);
		fseek(pFile, sizeVBO * sizeof(PixelData), SEEK_CUR);

		fread(&sizeIdx, sizeof(sizeIdx), 1, pFile);
		fseek(pFile, sizeIdx * sizeof(std::uint16_t), SEEK_CUR);
	}

	/* Read object data */ {
		std::uint32_t num;
		std::uint8_t stringSize;

		fread(&num, sizeof(num), 1, pFile);

		for(std::uint32_t i = 0; i < num; ++i) {
			/* Name */ {
				fread(&stringSize, sizeof(stringSize), 1, pFile);
				fseek(pFile, stringSize, SEEK_CUR);
			}

			/* FirstLast Idx and 4 textures */ {
				fseek(pFile, sizeof(std::uint16_t) * 2 + sizeof(std::uint8_t) * 4, SEEK_CUR);
			}
		}
	}

	this->loadCustomFile(pFile);
	fclose(pFile);
}

#include <utils/fps.h>
void ImageArray::loadCustomFile(FILE *pFile) {
	Fps f; f.begin();

	std::uint32_t numTexture;
	fread(&numTexture, sizeof(numTexture), 1, pFile);

	dds::CDDSImage imgObj;

	setupThrow(LOG, imgObj.load(pFile, false), SSTR("Load first file @ " << ftell(pFile)));

	this->mipMapNb = imgObj.get_num_mipmaps();
	this->internalFormat = imgObj.get_internal_format();
	this->width = imgObj.get_width();
	this->height = imgObj.get_height();

	LOG.info(SSTR("Start loading ImageArray !\tSize :" << this->height << "x" << this->width << " x" << numTexture));

	(*this)();

	bool mipMapFull = true;

	unsigned int i = 0;
	for(i = 0; i < numTexture; i++) {
		if(i == 0) {
			glTexStorage3D(GL_TEXTURE_2D_ARRAY,
						   this->mipMapNb+1,
						   this->internalFormat,
						   this->width,
						   this->height,
						   numTexture);
		} else {
			setupThrow(LOG, imgObj.load(pFile, false), "Load next file");
		}

		if(this->mipMapNb != imgObj.get_num_mipmaps())
			mipMapFull = false;

		glCompressedTexSubImage3D(
					GL_TEXTURE_2D_ARRAY,
					0,								// Level
					0, 0, i,
					this->width, this->height, 1,
					this->internalFormat,
					imgObj.get_size(),
					imgObj);

		for(unsigned int j = 0; j < imgObj.get_num_mipmaps(); j++) {
			dds::CSurface mipmap = imgObj.get_mipmap(j);

			glCompressedTexSubImage3D(
						GL_TEXTURE_2D_ARRAY,
						j+1,
						0, 0, i,
						mipmap.get_width(), mipmap.get_height(), 1,
						imgObj.get_internal_format(),
						mipmap.get_size(),
						mipmap);
		}
	}

	if(!mipMapFull) {
		LOG.info("Generate MIPMAP");
		glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	}

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	LOG.done(SSTR("Full loading done in " << (int)(f.end()*1000) << " ms"));
}

void ImageArray::load() {
	Fps f;

	f.begin();
	LOG.info(SSTR("Start loading ImageArray !\tSize :" << this->height << "x" << this->width << " x" << this->imageList.size()));

	(*this)();

	bool mipMapFull = true;

	glTexStorage3D(GL_TEXTURE_2D_ARRAY,
				   this->mipMapNb+1,
				   this->internalFormat,
				   this->width,
				   this->height,
				   this->imageList.size());

	unsigned int i = 0;
	dds::CDDSImage imgObj;
	for(auto img : this->imageList) {
		//		imgObj.load(img.first, img.second);
		setupThrow(LOG, imgObj.load(img.first, img.second), SSTR("Load file " << img.first));

		if(this->mipMapNb != imgObj.get_num_mipmaps())
			mipMapFull = false;

		glCompressedTexSubImage3D(
					GL_TEXTURE_2D_ARRAY,
					0,								// Level
					0, 0, i,
					this->width, this->height, 1,
					this->internalFormat,
					imgObj.get_size(),
					imgObj);

		for(unsigned int j = 0; j < imgObj.get_num_mipmaps(); j++) {
			dds::CSurface mipmap = imgObj.get_mipmap(j);

			glCompressedTexSubImage3D(
						GL_TEXTURE_2D_ARRAY,
						j+1,
						0, 0, i,
						mipmap.get_width(), mipmap.get_height(), 1,
						imgObj.get_internal_format(),
						mipmap.get_size(),
						mipmap);
		}

		i++;
	}

	if(!mipMapFull) {
		LOG.info("Generate MIPMAP");
		glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	}

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	LOG.done(SSTR("Full loading done in " << (int)(f.end()*1000) << " ms"));
}

void ImageArray::operator ()() {
	glBindTexture(GL_TEXTURE_2D_ARRAY, this->txtId);
}
