#include "mtlloader.h"

#include <fstream>

#include <utils/fps.h>
#include <utils/txtutils.h>

SETUP_LOG(MtlLoader, "LOADER-MTL")
const MTL MtlLoader::def({glm::vec3(1, 1, 1), ""});

#include <iostream>
MtlLoader::MtlLoader() : it(this->lst.end()){}

bool MtlLoader::load(std::string filePath) {
	Fps f;

	std::ifstream file(filePath);

	setupNoRet(LOG, file.is_open(), "Opening file " + filePath, return false)

	std::string workingLabel;
	MTL workingItem = {glm::vec3(1, 1, 1), ""};

	std::string line;
	while(file >> line) {
		if(line.at(0) == '#') {
			getline(file, line);
			continue;
		}

		if(line.compare("newmtl") == 0) {
			if(!workingLabel.empty())
				this->lst.insert({workingLabel, workingItem});

			workingItem = {glm::vec3(1, 1, 1), ""};
			file >> workingLabel;
		} else

		if(line.compare("Kd") == 0) { // Difuse color
			file >> workingItem.Kd[0];
			file >> workingItem.Kd[1];
			file >> workingItem.Kd[2];
		} else if(line.compare("map_Kd") == 0) { // Texture
			getline(file, workingItem.mapKd);

			trim(workingItem.mapKd);

			if(!workingItem.mapKd.empty())
				this->imgBuffer.insert({workingItem.mapKd, new Image(filePath.substr(0, filePath.find_last_of("/")+1) + workingItem.mapKd)});
		} else
			getline(file, line);
	}


	if(!workingLabel.empty())
		this->lst.insert({workingLabel, workingItem});

	LOG.done(SSTR("Loading done in " << 1000*f.end() << "ms"));

	return true;
}

MtlLoader::~MtlLoader() {
	for(const auto& i : this->imgBuffer)
		delete i.second;
}

bool MtlLoader::lockItem(std::string s) {
	this->it = this->lst.find(s);

	return (this->it != this->lst.end());
}

MTL MtlLoader::getMaterial() {
	if(this->it == this->lst.end())
		return MtlLoader::def;

	return this->it->second;
}

glm::vec3 MtlLoader::getColor() {
	return this->getMaterial().Kd;
}

Image* MtlLoader::getImage() {
	if(this->getMaterial().mapKd.empty()) return NULL;

	return this->imgBuffer[this->getMaterial().mapKd];
}
