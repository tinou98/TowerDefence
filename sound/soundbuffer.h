#ifndef SOUNDBUFFER_H
#define SOUNDBUFFER_H

#include <utils/log.h>
#include <sndfile.hh>
#include <al.h>
#include <alc.h>

#include <unordered_set>
#include <string>

namespace Sound {
	class Source;

	class Buffer {
		friend class Source;
	public:
		explicit Buffer(const std::string &fileName);

		static ALenum getFormat(int channel);

	protected:
		ADD_DEFAULT_LOG
		SndfileHandle file;

		std::unordered_set<ALuint> srcLst;

		virtual ALuint addSource(Source *s);
		virtual ALuint remSource(Source *s);
	};
}

#endif // SOUNDBUFFER_H
