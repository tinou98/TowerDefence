#include "soundbuffersmall.h"

#include <vector>
#include <algorithm>

using namespace Sound;

SETUP_LOG(BufferSmall, "SOUND-BUFFER-SMALL")

ALshort avg(ALshort* arr, unsigned int from, unsigned int lenght) {
	ALdouble ret = 0;

	for(unsigned int i = 0; i < lenght; i++)
		ret += ((ALdouble)arr[from + i])/lenght;

	return ret;
}

BufferSmall::BufferSmall(std::string fileName, bool convertToMono) : Sound::Buffer(fileName) {
	// Lecture des échantillons audio au format entier 16 bits signé (le plus commun)
	ALsizei nbSample = this->file.channels() * this->file.frames();
	ALshort *samples = new ALshort[nbSample];

	setup(LOG, file.read(&samples[0], nbSample), "Reading file")

	ALexec(LOG, alGenBuffers(1, &this->buffer));

	if(convertToMono && this->file.channels() != 1) {
		for(int i = 0; i < this->file.frames(); ++i)
			samples[i] = avg(samples, i*this->file.channels(), this->file.channels());

		ALexec(LOG, alBufferData(this->buffer, Sound::Buffer::getFormat(1), &samples[0], this->file.frames() * sizeof(ALshort), this->file.samplerate()));
	} else {
		ALexec(LOG, alBufferData(this->buffer, Sound::Buffer::getFormat(file.channels()), &samples[0], nbSample * sizeof(ALushort), this->file.samplerate()));
	}

	delete[] samples;
}

BufferSmall::~BufferSmall() {
	for(ALuint sId : this->srcLst) {
		ALexec(LOG, alSourceStop(sId));
		ALexec(LOG, alSourcei(sId, AL_BUFFER, 0));
	}

	ALexec(LOG, alDeleteBuffers(1, &this->buffer));
}

#include "soundsource.h"
ALuint BufferSmall::addSource(Source *s) {
	ALuint sId = Buffer::addSource(s);
	ALexec(LOG, alSourcei(sId, AL_BUFFER, this->buffer));

	return sId;
}

ALuint BufferSmall::remSource(Source *s) {
	ALuint sId = Buffer::remSource(s);
	ALexec(LOG, alSourcei(sId, AL_BUFFER, 0));

	return sId;
}
