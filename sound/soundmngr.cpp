#include "soundmngr.h"

#include <stdexcept>
#include <cstring>

SETUP_LOG(Sound::Mngr, "SOUND-MANAGER")

Sound::Mngr::Mngr(ALCchar *name) {
	setupThrow(LOG, this->device = alcOpenDevice(name), SSTR("Open device : " << name))
	setupThrow(LOG, this->ctx = alcCreateContext(this->device, NULL), "Create Context")
	setupThrow(LOG, alcMakeContextCurrent(this->ctx), "Activating Context")


	LOG.info(SSTR("Version: " << alGetString(AL_VERSION)));
	LOG.info(SSTR("Vendor: " << alGetString(AL_VENDOR)));
	LOG.info(SSTR("Renderer: " << alGetString(AL_RENDERER)));
}

Sound::Mngr::~Mngr() {
	setupNoRet(LOG, alcMakeContextCurrent(NULL), "Desactivating Context",)
	alcDestroyContext(this->ctx);LOG.done("Delete Context");
	setupNoRet(LOG, alcCloseDevice(this->device), "Close device",)

}

std::string Sound::Mngr::deviceDefault() {
	return std::string(alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER));
}

std::vector<std::string> Sound::Mngr::listDevices() {
	std::vector<std::string> ret;
	const ALCchar* deviceList = alcGetString(NULL, ALC_DEVICE_SPECIFIER);

	if(deviceList) {
		// Extracting devices
		while(strlen(deviceList) > 0) {
			ret.push_back(deviceList);
			deviceList += strlen(deviceList) + 1;
		}
	}

	return ret;
}

#include <sstream>
#include <algorithm>
#include <iterator>
#include <iostream>
std::vector<std::string> Sound::Mngr::listExtension() {
	const ALCchar* ext = alGetString(AL_EXTENSIONS);

	std::vector<std::string> ret;
	std::stringstream ss(ext);
	std::string item;
	while (std::getline(ss, item, ' ')) {
		ret.push_back(item);
	}

	return ret;
}

bool Sound::Mngr::isExtensionPresent(std::string ext) {
	return (alIsExtensionPresent(ext.c_str()) == AL_TRUE);
}
