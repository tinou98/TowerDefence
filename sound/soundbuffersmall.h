#ifndef SOUNDBUFFERSMALL_H
#define SOUNDBUFFERSMALL_H

#include "soundbuffer.h"

namespace Sound {

class BufferSmall : public Buffer {
public:
	BufferSmall(std::string fileName, bool convertToMono = true);
	~BufferSmall();

	ALuint addSource(Source *s);
	ALuint remSource(Source *s);

private:
	ADD_DEFAULT_LOG

	Source *s;
	ALuint buffer;
};

}

#endif // SOUNDBUFFERSMALL_H
