#ifndef SOUNDSOURCE_H
#define SOUNDSOURCE_H

#include <utils/log.h>

#include <al.h>
#include <alc.h>

#include "soundbuffer.h"

namespace Sound {

	class Source {
		friend class Buffer;
	public:
		Source();
		explicit Source(Buffer *buff);
		~Source();
		void setBuffer(Buffer *buffer);

		void play();
		void pause();
		void stop();


	protected:
		ADD_DEFAULT_LOG
		operator ALuint() {return this->srcId; }

	private:
		ALuint srcId;

		Buffer *buffer = 0;
	};

}

#endif // SOUNDSOURCE_H
