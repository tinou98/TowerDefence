#include "soundbuffer.h"

#include "soundsource.h"

using namespace Sound;

SETUP_LOG(Buffer, "SOUND-BUFFER")

Buffer::Buffer(const std::string &fileName) : file(fileName) {
	setup(LOG, file, SSTR("Opening sound file " << fileName))
}


ALuint Buffer::addSource(Source *s) {
	this->srcLst.insert(s->operator ALuint());

	return s->operator ALuint();
}

ALuint Buffer::remSource(Source *s) {
	this->srcLst.erase(s->operator ALuint());

	return s->operator ALuint();
}

ALenum Buffer::getFormat(int channel) {
	ALenum format;
	switch (channel) {
		case 1:  format = AL_FORMAT_MONO16;                    break;
		case 2:  format = AL_FORMAT_STEREO16;                  break;
		case 4:  format = alGetEnumValue("AL_FORMAT_QUAD16");  break;
		case 6:  format = alGetEnumValue("AL_FORMAT_51CHN16"); break;
		case 7:  format = alGetEnumValue("AL_FORMAT_61CHN16"); break;
		case 8:  format = alGetEnumValue("AL_FORMAT_71CHN16"); break;
		default: format = 0;                                   break;
	}

	if(format == -1) format = 0;

	return format;
}

