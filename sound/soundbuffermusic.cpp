#include "soundbuffermusic.h"

using namespace Sound;

SETUP_LOG(BufferMusic, "SOUND-BUFFER-MUSIC")
std::unordered_set<BufferMusic*> BufferMusic::managedObject;
std::thread* BufferMusic::internalThread = 0;
std::mutex BufferMusic::mutex;


BufferMusic::BufferMusic(const std::string &fileName) : Buffer(fileName), chunk(file.samplerate() * file.channels() / 20), sampleRate(file.samplerate()) {
	// Generate & add empty buffer
	ALexec(LOG, alGenBuffers(this->bufferLst.size(), &this->bufferLst[0]));

	ALshort samples[chunk];


	for(int i = 0; i < this->bufferLst.size(); i++) {
		ALsizei readed = 0;
		setup(LOG, readed = file.read(samples, chunk), SSTR("Reading audio file"));

		ALexec(LOG, alBufferData(this->bufferLst[i], Buffer::getFormat(file.channels()), samples, readed * sizeof(ALushort), sampleRate));
	}


	// Add to thread loop
	mutex.lock();
	BufferMusic::managedObject.insert(this);
	mutex.unlock();

	if(BufferMusic::internalThread == 0)
		BufferMusic::internalThread = new std::thread(BufferMusic::threadedFunc);
}

BufferMusic::~BufferMusic() {
	mutex.lock();
	BufferMusic::managedObject.erase(this);
	mutex.unlock();

	for(ALuint id : this->srcLst)
		stopAndRemSource(id);


	ALexec(LOG, alDeleteBuffers(this->bufferLst.size(), &this->bufferLst[0]));
}


ALuint BufferMusic::addSource(Source *s) {
	ALuint sId = Buffer::addSource(s);

	ALexec(LOG, alSourceQueueBuffers(sId, this->bufferLst.size(), &this->bufferLst[0]));
	return sId;
}

ALuint BufferMusic::remSource(Source *s) {
	ALuint sId = Buffer::remSource(s);

	stopAndRemSource(sId);
	return sId;
}

void BufferMusic::stopAndRemSource(ALuint sId) {
	ALexec(LOG, alSourceStop(sId));

	ALint nbDone = 0;
	ALexec(LOG, alGetSourcei(sId, AL_BUFFERS_PROCESSED, &nbDone));

	ALuint tmpList[nbDone];
	ALexec(LOG, alSourceUnqueueBuffers(sId, nbDone, tmpList));
}

#include <chrono>
#include <iostream>

void BufferMusic::internalThreadProcess() {
	if(this->srcLst.empty())
		return;

	ALint nbDone = 0;
	ALexec(LOG, alGetSourcei(*this->srcLst.begin(), AL_BUFFERS_PROCESSED, &nbDone));


	if(nbDone != 0) {
//		ALshort *samples = new ALshort[chunk];
		ALshort samples[chunk];

		ALuint buffer[nbDone];
		for(ALint id : this->srcLst)
			ALexec(LOG, alSourceUnqueueBuffers(id, nbDone, buffer));

		for(int i = 0; i < nbDone; i++) {
			ALsizei readed = 0;
			setup(LOG, readed = file.read(samples, chunk), SSTR("Reading audio file"));

//			LOG.info(SSTR(buffer[i] << " " << file.channels() << " " << Buffer::getFormat(file.channels()) << " " << samples << " " << readed * sizeof(ALshort) << " " << sampleRate));
			ALexec(LOG, alBufferData(buffer[i], Buffer::getFormat(file.channels()), samples, readed * sizeof(ALushort), sampleRate));
		}

		for(ALint id : this->srcLst) {
			ALexec(LOG, alSourceQueueBuffers(id, nbDone, buffer));
		}
	}
}

void BufferMusic::threadedFunc() {
	while(!BufferMusic::managedObject.empty()) {
		{
			mutex.lock();
			for(BufferMusic* e : BufferMusic::managedObject)
				e->internalThreadProcess();
			mutex.unlock();
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	BufferMusic::internalThread = 0;
}
