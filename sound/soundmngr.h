#ifndef SOUNDMNGR_H
#define SOUNDMNGR_H

#include <utils/log.h>

#include <al.h>
#include <alc.h>

#include <vector>
#include <string>

namespace Sound {

class Mngr
{
public:
	explicit Mngr(ALCchar *name = NULL);
	~Mngr();

	static std::string deviceDefault();
	static std::vector<std::string> listDevices();
	static std::vector<std::string> listExtension();
	static bool isExtensionPresent(std::string ext);
private:
	ADD_DEFAULT_LOG

	ALCdevice* device;
	ALCcontext* ctx;
};

}

#endif // SOUNDMNGR_H
