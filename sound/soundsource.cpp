#include "soundsource.h"

using namespace Sound;
SETUP_LOG(Source, "SOUND-SOURCE")

#include <iostream>
Source::Source() {
	ALexec(LOG, alGenSources(1, &this->srcId));
}

Source::Source(Buffer *buff) : Source() {
	this->setBuffer(buff);
}

Source::~Source() {
	this->stop();

	if(this->buffer != 0)
		this->buffer->remSource(this);

	ALexec(LOG, alDeleteSources(1, &this->srcId));
}

void Source::setBuffer(Buffer *buffer) {
	this->stop();

	if(this->buffer != 0)
		this->buffer->remSource(this);

	this->buffer = buffer;

	if(this->buffer != 0)
		this->buffer->addSource(this);
}

void Source::play() {
	ALexec(LOG, alSourcePlay(*this));
}

void Source::pause() {
	ALexec(LOG, alSourcePause(*this));
}

void Source::stop() {
	ALexec(LOG, alSourceStop(*this));
}

