#ifndef SOUNDBUFFERMUSIC_H
#define SOUNDBUFFERMUSIC_H

#include "soundbuffer.h"
#include <utils/log.h>

#include <vector>
#include <unordered_set>
#include <array>
#include <thread>
#include <mutex>

#include "soundsource.h"

namespace Sound {

class BufferMusic : public Buffer {
public:
	explicit BufferMusic(const std::string &fileName);
	~BufferMusic();


private:
	ADD_DEFAULT_LOG

	void stopAndRemSource(ALuint sId);

	ALuint addSource(Source *s);
	ALuint remSource(Source *s);

	std::array<ALuint, 4> bufferLst;

	const ALsizei chunk;
	const ALsizei sampleRate;


	void internalThreadProcess();
	static void threadedFunc();
	static std::unordered_set<BufferMusic*> managedObject;
	static std::thread *internalThread;
	static std::mutex mutex;
};

}

#endif // SOUNDBUFFERMUSIC_H
