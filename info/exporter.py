import bpy
import bgl
from mathutils import Vector
from bpy_extras.io_utils import ExportHelper
import bmesh
from bpy.types import UIList 

from copy import copy
import re
import os

import ddsExporter
import utils
pck = utils.Packer

class ExportSomeData(bpy.types.Operator, ExportHelper):
    """Export in the format use by TD engine"""
    bl_idname = "binary_td.exporter"
    bl_label = "Binary File (Game)"
    
    # ExportHelper mixin class uses this
    filename_ext = ".b"
    
    filter_glob = bpy.props.StringProperty(
        default="*.b",
        options={'HIDDEN'},
    )
    
    def execute(self, context):
        rxDataPath = re.compile(r'pose\.bones\["(.*)"\]\.(.*)')
        
        textureSaveOrder = [];
        objLstSize = 0;
        
        # Parsing data
        objLst = list();
        vbo = list();
        idx = list();
        
        for o in bpy.data.objects:
            if o.exportable:
                if len(objLst): # not first object
                    objLst[-1]['end'] = len(idx)-1
                    
                print("\tStart export object: ", o.name)
                
                armObj = utils.getObject(o)['armatureObj']
                arm = utils.getObject(o)['armature']
                
                currObj = {
                    'name':         o.name,
                    'start':        len(idx),
                    'end':          -1,
                    'textures':  [
                        o.texture.diffuse,  #   Diffuse
                        o.texture.glossy,   #   Glossy
                        o.texture.normal,   #   Normal
                        o.texture.bump,     #   Bump
                    ],
                    'bones':    {
                        'bonesRelation':    [utils.getId(arm, b.parent if b.parent is not None else b) for b in arm.bones], # Bones Rel : [parent]
                        'pose':             [],                                                                             # Pose Data : [[poseName, {boneId, boneQuaternion}]]
                        'animation':        [],                                                                             # Animation : [[animName, duration, {time, poseId}]]
                    },
                }
                objLstSize += 1 + len(currObj['name']) + 2*2 + 4*1
                
                for text in currObj['textures']:
                    if text == "":
                        continue;
                    
                    try:
                        textureSaveOrder.index(text)
                    except ValueError:
                        textureSaveOrder.append(text)
                    
                objLstSize += len(currObj['bones']['bonesRelation'])
                for act in o.actions:
                    action = bpy.data.actions[act.name]
                    
                    rng = action.frame_range
                    currAnim = [action.name, (rng[1] - rng[0]) / context.scene.render.fps, {}]
                    for pose in action.pose_markers:
                        name = pose.name if pose.name != "Pose" else ""
                        frame = pose.frame
                        
                        currPose = [name, {}]
                        for fc in action.fcurves:
                            valList = [p.co[1] for p in fc.keyframe_points if p.co[0] == frame]
                            if len(valList) != 1:
                                continue
                            
                            value = valList[0]
                            
                            # Add contrainst
                            match = rxDataPath.match(fc.data_path)
                            
                            if match:
                                boneName, contrainstType = match.group(1, 2)
                            else:
                                raise RuntimeError("Unable to parse data path : " + fc.data_path)
                            
                            boneId = utils.getId(arm, arm.bones[boneName])
                            if boneId not in currPose:
                                currPose[1][boneId] = [[float('NaN') for i in range(j)] for j in [3, 4, 3]]
                            
                            typeId = {
                                "location": 0,
                                "rotation_quaternion": 1,
                                "scale": 2,
                            }.get(contrainstType, -1)
                            
                            if typeId == -1:
                                raise RuntimeError("Unknow data path target : " + contrainstType)
                                
                            currPose[1][boneId][typeId][fc.array_index] = value
                        
                        currAnim[2][frame] = len(currObj['bones']['pose'])
                        currObj['bones']['pose'].append(currPose)
                        objLstSize += 1 + len(currPose[0]) + len(currPose[1])*(1+3+4+3)
                    
                    currObj['bones']['animation'].append(currAnim)
                    objLstSize += 1 + len(currAnim[0]) + 4 + 1 + len(currAnim[2])*(4+1)
                
                
                bm = bmesh.new()
                bm.from_mesh(o.data)
                bmesh.ops.triangulate(bm, faces=bm.faces)
                    
                try:
                    uv_layer = bm.loops.layers.uv[o.texture.uvMap]
                    if uv_layer is None:
                        raise Exception("No active UV map (uv)")
                except:
                    print("No active UV map (uv)")

                for face in bm.faces:
                    for l in face.loops:
                        xyz = copy(l.vert.co)
                        
                        uv = copy(l[uv_layer].uv if o.texture.uvMap != "" else (0, 0))
                        
                        vrtxGrp = utils.getBonesLinkedBM(l, bm, o)
                        
                        # Resize to 4 : expand, order, then shrink
                        while(len(vrtxGrp) < 4):
                            vrtxGrp.append((0, 0))
                            
                        vrtxGrp.sort(reverse = True, key = lambda t: t[1])
                            
                        while(len(vrtxGrp) > 4):
                            vrtxGrp.pop()
                            
                        # Normalisation
                        bonId = [t[0] for t in vrtxGrp];
                        bonW = [t[1] for t in vrtxGrp];
                            
                        tot = sum(bonW)
                        bonW = [x / tot for x in bonW]
                        
                        try:
                            idx += [vbo.index((xyz, uv, bonId, bonW))]
                        except:
                            idx += [len(vbo)]
                            vbo += [(xyz, uv, bonId, bonW)]

                bm.free()
                del bm
                
                objLst.append(currObj)
        
        # Add last vertex to list
        if len(objLst):
            objLst[-1]['end'] = len(idx)-1
        
        with open(self.filepath, 'wb') as f:
            #write header
            f.write(b'TD');
            
            offset = 2 + (8+4)*4;
            
            f.write(pck.ulonglong(offset)) # Offest
            f.write(pck.uint(len(objLst))) # Size
            offset += objLstSize
            
            f.write(pck.ulonglong(offset))
            f.write(pck.uint(len(vbo)))
            offset += len(vbo) * (3+2+4+4) * 4
            
            f.write(pck.ulonglong(offset))
            f.write(pck.uint(len(idx)))
            offset += len(idx) * 2
            
            f.write(pck.ulonglong(offset))
            f.write(pck.uint(len(textureSaveOrder)))
            
            for obj in objLst:
                f.write(pck.str(obj['name']))
                
                f.write(pck.ushort(obj['start']))
                f.write(pck.ushort(obj['end']))
                
                for i in range(4):
                    if obj['textures'][i] != "":
                        try:
                            f.write(pck.ubyte(textureSaveOrder.index(textureSaveOrder[i]) + 1))
                        except:
                            raise RuntimeError("Texture should already be here ...");
#                            textureSaveOrder += [textArr[i]]
#                            f.write(pck.ubyte(len(textureSaveOrder)))
                    else:
                        f.write(pck.ubyte(0))
                
                
                f.write(pck.ubyte(len(obj['bones']['bonesRelation'])))
                # Write bones parent
                for id in obj['bones']['bonesRelation']:
                    f.write(pck.ubyte(id));
                
                f.write(pck.ubyte(len(obj['bones']['pose'])))
                # Write poses
                for poseName, constraint in obj['bones']['pose']:
                    f.write(pck.str(poseName));
                    f.write(pck.ubyte(len(constraint)));
                    for id, pos in constraint.items():
                        f.write(pck.ubyte(id))
                        f.write(pck.vec3(pos[0]))
                        f.write(pck.vec4(pos[1]))
                        f.write(pck.vec3(pos[2]))
                
                f.write(pck.ubyte(len(obj['bones']['animation'])))
                # Write animation
                for animName, duration, keyframes in obj['bones']['animation']:
                    f.write(pck.str(animName));
                    f.write(pck.float(duration));
                    f.write(pck.ubyte(len(keyframes)));
                    for currTime, poseId in keyframes.items():
                        f.write(pck.float(currTime));
                        f.write(pck.ubyte(poseId));
            
            
            for pixData in vbo:
                f.write(pck.pixelData(pixData));
            
            for id in idx:
                f.write(pck.ushort(id));
            
            
            # Write Texture
            headerSize = 4+124+32 #(+20)
            for textName in textureSaveOrder:
                image = bpy.data.images[textName]
                ddsFileName = ddsExporter.exportToDDS(image)
                    
                with open(ddsFileName, 'rb') as ddsFile:
                    f.write(ddsFile.read()[headerSize:])
                os.remove(ddsFileName)
            
            f.close()
            
            return {'FINISHED'}


# Only needed if you want to add into a dynamic menu
def create_menu(self, context):
    self.layout.operator(ExportSomeData.bl_idname, text=ExportSomeData.bl_label)


def register():
    bpy.utils.register_class(ExportSomeData)
    bpy.types.INFO_MT_file_export.append(create_menu)
    
    print("[LOAD] Exporter")


def unregister():
    bpy.utils.unregister_class(ExportSomeData)
    bpy.types.INFO_MT_file_export.remove(create_menu)


if __name__ == "__main__":
    register()
