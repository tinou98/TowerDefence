import bpy
import utils

def updateArmature(self, value):
    obj = utils.getObject(bpy.data.armatures[self.squeleton])["armature"]
    for i in range(20):
        obj.layers[i] = (i == 2)

def setBonesStyle(self, value):
    bpy.data.armatures[self.squeleton].show_bone_custom_shapes = not self.preview
    
def getBonesStyle(self):
    try:
        return bpy.data.armatures[self.squeleton].show_bone_custom_shapes
    except:
        return False


class bonesParam(bpy.types.PropertyGroup):
    squeleton = bpy.props.StringProperty(
    	name="Squeleton",
    	description="Bones list",
    	options=set(),
        update=updateArmature
    )
    
    preview = bpy.props.BoolProperty(
    	name="Preview bones shape",
    	description="Change Bones representation to be bounding box",
    	default=False,
        get=getBonesStyle,
        set=setBonesStyle,
    )



class BonesPanel(bpy.types.Panel):
    """Panneaux de configuration de l'armature """
    bl_label = "Bones"
    bl_idname = "binary_td.bones_panel"
#    bl_space_type = 'PROPERTIES'
#    bl_region_type = 'WINDOW'
#    bl_context = "object"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = ""

    @classmethod
    def poll(cls, context):
        return utils.autoPoll(cls, context)
    
    def draw(self, context):
        layout = self.layout
        obj = context.object
        
        layout.prop_search(obj.bones, "squeleton", bpy.data, "armatures")
        layout.prop(obj.bones, "preview")


############
# Per Bone #
############
class perBoneParam(bpy.types.PropertyGroup):
    collisionShape = bpy.props.EnumProperty(
        name="Shape",
        description="Choose collision shape for specific bones",
        items=[
            ("0", "None", ""),
        	("1", "BvhTriangleMeshShape", ""),
            ("2", "BoxShape", ""),
            ("3", "SphereShape", ""),
            ("4", "CapsuleShape", ""),
            ("5", "CylinderShape", ""),
            ("6", "ConeShape", ""),
            ("7", "ConvexHullShape", ""),
            ("8", "CompoundShape", ""),
        ],
    )
    
    mass = bpy.props.FloatProperty(
        name="Masse",
        description="Masse de l'os",
        default=1.0,
    )
    
    boxHalfExtents = bpy.props.FloatVectorProperty(
        name="Taille boite",
        description="Taille de la boite (en demi unite)",
        default=(0.5, 0.5, 0.5),
        subtype='XYZ',
    )
    
    radius = bpy.props.FloatProperty(
        name="Rayon",
#        description="",
    )
    
    height = bpy.props.FloatProperty(
        name="Hauteur",
#        description="",
    )
        

class perBonePanel(bpy.types.Panel):
    """Panneaux de configuration d'un OS """
    bl_label = "Per Bone"
    bl_idname = "binary_td.perBone_panel"
#    bl_space_type = 'PROPERTIES'
#    bl_region_type = 'WINDOW'
#    bl_context = "object"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = ""
    
    @classmethod
    def poll(cls, context):
        if (context.active_bone is None) or not isinstance(context.active_bone, bpy.types.Bone):
            return False;
        
        try:
            obj = utils.getObject(context.object)["object"];
            return obj.exportable;
        except:
            return False
    
    def draw(self, context):
        layout = self.layout
        obj = context.active_bone
        
        #bone = context.bone
        #if not bone:
        #    bone = context.edit_bone
        
        layout.prop(obj.perBone, "collisionShape")
        layout.prop(obj.perBone, "mass")
        
        if obj.perBone.collisionShape == '1':
            pass;
        elif obj.perBone.collisionShape in {'2', '5'}:
            layout.column().prop(obj.perBone, "boxHalfExtents")
        elif obj.perBone.collisionShape == '3':
            layout.prop(obj.perBone, "radius")
        elif obj.perBone.collisionShape in {'4', '6'}:
            layout.prop(obj.perBone, "radius")
            layout.prop(obj.perBone, "height")
        elif obj.perBone.collisionShape == '7':
            pass;
        elif obj.perBone.collisionShape == '8':
            pass;
        else:
            pass;
        # Add object and
        # hide_select
        # draw_type = "Wire"


def register():
    bpy.utils.register_class(BonesPanel)
    bpy.utils.register_class(bonesParam)
    bpy.types.Object.bones = bpy.props.PointerProperty(type=bonesParam)
    
    bpy.utils.register_class(perBonePanel)
    bpy.utils.register_class(perBoneParam)
    bpy.types.Bone.perBone = bpy.props.PointerProperty(type=perBoneParam)

    print("[LOAD] Bones")

def unregister():
    bpy.utils.unregister_class(BonesPanel)
    bpy.utils.unregister_class(bonesParam)
    
    bpy.utils.unregister_class(perBonePanel)
    bpy.utils.unregister_class(perBoneParam)

if __name__ == "__main__":
    register()
