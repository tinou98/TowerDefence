import bpy

import subprocess
import os
import tempfile

converter = [
    "C:/Program Files (x86)/NVIDIA Corporation/DDS Utilities/nvdxt.exe",
    "nvcompress",
]

option = [
    ["-dxt5", "-quality_highest", "-flip", "-rescale", "nearest", "-file", "{0}", "-outsamedir"],
#    ["-dxt5", "-quality_highest", "-flip", "-rescale", "nearest", "-file", "{0}", "-output", "{1}"],
    ["-bc3", "{0}", "{1}"],
]

convIdx = None;

def exportToDDS(image, fileName = None):
    if fileName is None:
        fileName = tempfile.NamedTemporaryFile(delete=False).name;
    
    global converter
    global option
    global convIdx
    
    if convIdx is None:
        # Check wich DDS utils to use
        for id, c in enumerate(converter):
            try:
                subprocess.call([c])
            except:
                print("[ ... ] NOT found", c)
            else:
                convIdx = id
                print("[ O K ] Found :", c)
                break
    
    if convIdx == None:
        print("[ABORT] No converter found")
        return
    
    
    fileNameExt = fileName + bpy.context.scene.render.file_extension
    image.save_render(fileNameExt)
    
    command = [converter[convIdx]] + option[convIdx]
    for i in range(len(command)):
        command[i] = command[i].format(fileNameExt, fileName + ".dds")
    
    print("[ ... ] Command : ", command)
    subprocess.call(command)
    print("[ O K ] Done")
    os.remove(fileNameExt)
    
    return fileName + ".dds"