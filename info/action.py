import bpy

import utils

class ActionsList(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Name of actions", default="")

class OBJECT_UL_ActionList(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        if item.name == "":
            layout.label(text="<None>", icon='ERROR')
        else:
            try:
                action = bpy.data.actions[item.name];
                layout.prop(item, "name", text="", emboss=False, icon_value=115)
            except:
                layout.prop(item, "name", text="", emboss=False, icon_value=2)

class NewActionSlotOp(bpy.types.Operator):
    bl_idname = "action.add_to_gameengine"
    bl_label = "Add new action to current object"

    def execute(self, context):
        obj = utils.getObject(context.object)["object"];
        obj.actions.add();
        obj.active_action_index = len(context.object.actions) -1;
        return {'FINISHED'}
    
class DelActionSlotOp(bpy.types.Operator):
    bl_idname = "action.rem_from_gameengine"
    bl_label = "Remove action from current object"

    def execute(self, context):
        obj = utils.getObject(context.object)["object"];
        obj.actions.remove(context.object.active_action_index);
        obj.active_action_index = min(context.object.active_action_index, len(context.object.actions) -1, 0);
        return {'FINISHED'}
    
class ApplyCurrActionOp(bpy.types.Operator):
    bl_idname = "action.apply"
    bl_label = "Apply selected action"

    def execute(self, context):
        fullData = utils.getObject(context.object);
        obj = fullData["object"];
        armatureObj = fullData["armatureObj"];
        try:
            action = bpy.data.actions[obj.actions[obj.active_action_index].name];
        except:
            action = None;
        armatureObj.animation_data.action = action
        return {'FINISHED'}

class NewActionOp(bpy.types.Operator):
    bl_idname = "action.create"
    bl_label = "Add new action"
    
    def execute(self, context):
        bpy.data.actions.new(name="Action_" + utils.getObject(context.object)["object"].name)
        return {'FINISHED'}

class ActionsPanel(bpy.types.Panel):
    bl_label = "Actions"
    bl_idname = "OBJECT_PT_actions_select"
#    bl_space_type = 'PROPERTIES'
#    bl_region_type = 'WINDOW'
#    bl_context = "object"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = ""

    @classmethod
    def poll(cls, context):
        if context.object is None:
            return False;
        
        try:
            obj = utils.getObject(context.object)["object"];
            return obj.exportable;
        except:
            return utils.autoPoll(cls, context)

    def draw(self, context):
        layout = self.layout

        obj = utils.getObject(context.object)["object"];

        row = layout.row()
        row.template_list("OBJECT_UL_ActionList", "", obj, "actions", obj, "active_action_index")
        
        col = row.column(align=True)
        col.operator("action.add_to_gameengine", icon='ZOOMIN', text="")
        col.operator("action.rem_from_gameengine", icon='ZOOMOUT', text="")
        col.operator("action.apply", icon='ACTION', text="")

        row = layout.row(align=True)
        row.prop_search(obj.actions[obj.active_action_index], "name", bpy.data, "actions", text="")
        row.operator("action.create", icon='ZOOMIN', text="")


def register():
    bpy.utils.register_class(OBJECT_UL_ActionList)
    bpy.utils.register_class(ActionsList)
    bpy.utils.register_class(ActionsPanel)
    bpy.utils.register_class(NewActionOp)
    bpy.utils.register_class(NewActionSlotOp)
    bpy.utils.register_class(DelActionSlotOp)
    bpy.utils.register_class(ApplyCurrActionOp)
    
    bpy.types.Object.actions = bpy.props.CollectionProperty(type=ActionsList)
    bpy.types.Object.active_action_index = bpy.props.IntProperty()
    
    print("[LOAD] Actions")


def unregister():
    bpy.utils.unregister_class(OBJECT_UL_ActionList)
    bpy.utils.unregister_class(ActionsList)
    bpy.utils.unregister_class(ActionsPanel)
    bpy.utils.unregister_class(NewActionOp)
    bpy.utils.unregister_class(NewActionSlotOp)
    bpy.utils.unregister_class(DelActionSlotOp)
    bpy.utils.unregister_class(ApplyCurrActionOp)


if __name__ == "__main__":
    register()