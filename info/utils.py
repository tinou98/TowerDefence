import bpy
import struct

def getObject(obj):
    armatures = None;
    armaturesObj = None;
    try:        
        
        # If we get armatures data
        if isinstance(obj, bpy.types.Armature):
            armatures = obj;
            obj = None;
            for parent in bpy.data.objects:
                if parent.type == 'ARMATURE' and parent.data == armatures:
                    obj = parent;
                    break;

        if obj.type == 'ARMATURE':
            armaturesObj = obj;
            obj = None;
            for parent in bpy.data.objects:
                if parent.type == 'MESH' and parent.bones.squeleton == armaturesObj.data.name:
                    obj = parent;
                    break;

        if armaturesObj is None:
            for arm in bpy.data.objects:
                if arm.type == 'ARMATURE' and arm.data.name == obj.bones.squeleton:
                    armaturesObj = arm;
                    break;        
        
        if armatures is None:
            armatures = armaturesObj.data

        return {
            'object': obj,
            'armatureObj': armaturesObj,
            'armature': armatures,
        }
    except:
        return {
            'object': obj,
            'armatureObj': armaturesObj,
            'armature': armatures,
        }
        
        return {
            'object': None,
            'armatureObj': None,
            'armature': None,
        };


def autoPoll(cls, context):
    try:
        currObj = context.object
        return currObj.exportable;
    except:
        return False;
    

class Packer:
    @classmethod
    def ubyte(self, val):
        return struct.pack('B',val);

    @classmethod
    def ushort(self, val):
        return struct.pack('H',val);

    @classmethod
    def uint(self, val):
        return struct.pack('I',val);
    
    @classmethod
    def ulonglong(self, val):
        return struct.pack('Q',val);

    @classmethod
    def float(self, val):
        return struct.pack('f',val);

    @classmethod
    def vec2(self, val): 
        return self.float(val[0]) + self.float(val[1]);
    
    @classmethod
    def vec3(self, val): 
        return self.vec2(val) + self.float(val[2]);

    @classmethod
    def vec4(self, val): 
        return self.vec3(val) + self.float(val[3]);

    @classmethod
    def mat4(self, val):
        return self.vec4(val[0]) + self.vec4(val[1]) + self.vec4(val[2]) + self.vec4(val[3]);

    @classmethod
    def uvec2(self, val): 
        return self.uint(val[0]) + self.uint(val[1]);
    
    @classmethod
    def uvec3(self, val): 
        return self.uvec2(val) + self.uint(val[2]);

    @classmethod
    def uvec4(self, val): 
        return self.uvec3(val) + self.uint(val[3]);

    @classmethod
    def pixelData(self, val):
        #                  pos                 uv                bonesId           bonesWeight
        return self.vec3(val[0]) + self.vec2(val[1]) + self.uvec4(val[2]) + self.vec4(val[3])

    @classmethod
    def str(self, val):
        return self.ubyte(len(val)) + bytes(val, 'UTF-8')
    
    u8 = ubyte
    u16 = ushort
    u32 = uint
    u64 = ulonglong

    
def getBonesLinkedBM(vertex, bm, ob):
    grpLay = bm.verts.layers.deform.active
    
    return [(ob.vertex_groups[id].index, vertex.vert[grpLay][id]) for id in vertex.vert[grpLay].keys()]


def getId(obj, bone):
    for i in range(len(obj.bones)):
        if obj.bones[i] == bone:
            return i