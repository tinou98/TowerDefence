import bpy
import action
import bones
import texture
import utils
import exporter

def addObjectRightPanel(self, context):
    obj = context.object
    for i in range(20):
        obj.layers[i] = (i == 1)

class SetupPanel(bpy.types.Panel):
    bl_label = "Game Exporter"
    bl_idname = "binary_td.exp_setup"
#    bl_space_type = 'PROPERTIES'
#    bl_region_type = 'WINDOW'
#    bl_context = "object"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = ""

    @classmethod
    def poll(cls, context):
        return (context.object is not None)
    
    def draw_header(self, context):
        if context.object.type == 'MESH':
            self.layout.prop(context.object, "exportable", text="")
        
    def draw(self, context):
        layout = self.layout
        obj = context.object
        ok = False;
        try:
            if obj and obj.exportable:
                layout.label("Yes ! This object is exportable");
                ok = True;
            elif utils.getObject(obj)["object"].exportable:
                layout.label("Exported by object : " + utils.getObject(obj)["object"].name);
                ok = True;
            else:
                layout.label("Not exported");
        except:
            layout.label("Seems not exported ...");
        
        if ok:
            layout.operator("object.select_from_gameengine", icon='NONE', text="Select Objects")
            layout.operator("armature.select_from_gameengine", icon='NONE', text="Select armatures")


def select(c, o):
    #bpy.ops.object.select_all(action='DESELECT')
    for sel in c.selected_objects:
        sel.select = False;
    
    o.select = True
    c.scene.objects.active = o

class SelectArmaturesOp(bpy.types.Operator):
    bl_idname = "armature.select_from_gameengine"
    bl_label = "Select Armatures from current object by GameEngine option"

    def execute(self, context):
        select(context, utils.getObject(context.object)["armatureObj"])
        return {'FINISHED'}

class SelectObjectsOp(bpy.types.Operator):
    bl_idname = "object.select_from_gameengine"
    bl_label = "Select object from current Armatures by GameEngine option"

    def execute(self, context):
        select(context, utils.getObject(context.object)["object"])
        return {'FINISHED'}


def register():
    bpy.utils.register_class(SelectArmaturesOp)
    bpy.utils.register_class(SelectObjectsOp)
    
    bpy.utils.register_class(SetupPanel)
    bpy.types.Object.exportable = bpy.props.BoolProperty(name="Exportable objecqt", update=addObjectRightPanel)
    
    action.register();
    bones.register();
    texture.register();
    
    exporter.register();


def unregister():
    bpy.utils.runegister_class(SetupPanel)
    
    action.unregister();
    bones.unregister();
    texture.unregister();
    
    exporter.unregister();


if __name__ == "__main__":
    #unregister();
    register()