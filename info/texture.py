import bpy
import utils

###########
# Texture #
###########
class textureList(bpy.types.PropertyGroup):
    uvMap = bpy.props.StringProperty(
    	name="Carte UV",
    	description="Mappage UV a utiliser",
    	options=set(),
    )
    
    diffuse = bpy.props.StringProperty(
    	name="Diffuse",
    	description="Texture utilisé pour la composante diffuse",
    	options=set(),
    )
    
    glossy = bpy.props.StringProperty(
    	name="Glossy",
    	description="Texture utilisé pour la composante glossy (brillante)",
    	options=set(),
    )
    
    normal = bpy.props.StringProperty(
    	name="Normal",
    	description="Texture des veteur normaux",
    	options=set(),
    )
    
    bump = bpy.props.StringProperty(
    	name="Bump",
    	description="Texture utilisé pour la gestion automatique de qualité d'un mesh",
    	options=set(),
    )

class BakeParamPanel(bpy.types.Panel):
    """Panneaux de configuration des textures"""
    bl_label = "Textures Bake"
    bl_idname = "binary_td.prop_panel"
#    bl_space_type = 'PROPERTIES'
#    bl_region_type = 'WINDOW'
#    bl_context = "object"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_context = ""

    @classmethod
    def poll(cls, context):
        return utils.autoPoll(cls, context)
    
    def draw(self, context):
        layout = self.layout
        obj = context.object
        
        layout.prop_search(obj.texture, "uvMap", obj.data, "uv_textures")
        layout.prop_search(obj.texture, "diffuse", bpy.data, "images")
        layout.prop_search(obj.texture, "glossy", bpy.data, "images")
        layout.prop_search(obj.texture, "normal", bpy.data, "images")
        layout.prop_search(obj.texture, "bump", bpy.data, "images")


def register():
    bpy.utils.register_class(BakeParamPanel)
    bpy.utils.register_class(textureList)
    bpy.types.Object.texture = bpy.props.PointerProperty(type=textureList)

    print("[LOAD] Textures")

def unregister():
    bpy.utils.unregister_class(BakeParamPanel)
    bpy.utils.unregister_class(textureList)

if __name__ == "__main__":
    register()
