Internal file format
==========

Usage
===

This file is used to store meshes data, including :
 - Object name
 - Vertex data (including position, color, ...)
 - Index
 - Texture
 - Pose
 - Animation

File structure
=======

Header
---
File always start with the two letter **TD**.

Then followed by *4\** : (**Object**, **VBO**, **Index** and **Textures**)
- `uint64` Absolute offset
- `uint32` Size (in number of block)

Objects
----
For each object, we have : 
- `uint8` : *nameSize*
- `[char]` : name (of size *nameSize*)
- `uint16`*\*2* : first and last index
- `uint8` *\*4* : texture ID (Can be 0 if no texture) :
	- Diffuse
	- Specular
	- Normal
	- Bump / Height
- Bones Data :
	- `uint8` Number of Bones
	- For each bones
		- `uint8` Parent id (Use own `id` for root node, can have multiple root node)
	    - `mat4` Local matrice relative to parent
	- `uint8` Number of Poses
	- For each poses:
		- `uint8` *poseNameLenght*
		- `[char]` Pose Name
		- `uint8` Number of bones contrainsted
		- For each bones contrainsted in this pose :
			- `uint8` Bone ID
			- `float3` Position XYZ (use `NaN` if not set)
			- `float4` Rotation XYZW (use `NaN` if not set)
			- `float3` Scale XYZ (use `NaN` if not set)
	- `uint8` Number of Animation
	- For each animation :
		- `uint8` *animationNameLenght*
		- `[char]` Animation Name
		- `float` Total duration
		- `uint8` Keyframe number
		- For each keyframe :
			- `float` Current time
			- `uint8` Pose id

VBO & Index
------
This contain the two main buffer : 
1. The VBO content : 
	- `vec3` Position
	- `vec2` UV
	- `uvec4` Bones Index
	- `vec4` Bones Weight
2. `uint16` The index

Texture
-----
We start with all texture metadata :
- `uint32` Number of mipmap
- `uint32` internalFormat
- `uint32` width
- `uint32` height

Then for each texture :
- For each mipmap :
	- `[char]` RawData -> glCompressedTexSubImage3D

*All texture **must** have same size/mipmap levels (to be used in `TextureArray`)*
