#ifndef RENDERTOTEXTURE_H
#define RENDERTOTEXTURE_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <utils/log.h>

class renderToTexture {
public:
	explicit renderToTexture(int width = 1, int height = 1, bool depthTest = true, GLenum format = GL_RGB);
	~renderToTexture();

	void gen();
	void del();

	void resize(int width, int height);

	void bindToDraw();
	void bindTexture();
private:
	ADD_DEFAULT_LOG

	GLuint	frameBuffID = 0,
			renderedTextureID = 0,
			depthRenderID = 0;


	int width, height;
	bool depthTest;
	GLenum format;
};

class Locker {
public:
	explicit Locker(renderToTexture &obj) {
		glGetFloatv(GL_VIEWPORT, &prev[0]);

		obj.bindToDraw();
	}

	~Locker() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(prev[0], prev[1], prev[2], prev[3]);
	}

private:
	glm::vec4 prev;
};


#endif // RENDERTOTEXTURE_H
