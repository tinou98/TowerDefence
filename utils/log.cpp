#include "log.h"

#include <iostream>

Log::Log(const std::string &component) : component(component) {}

#include <iomanip>
#include <GLFW/glfw3.h>

#include <al.h>
#include <alc.h>

void Log::base() const {
	std::cout << std::setw(5) << std::fixed << glfwGetTime() << " [" << component << "] ";
}

void Log::init(const std::string &msg) const {
	this->base();
	std::cout << "\x1B[36m" << msg << "\x1B[0m" << std::endl;
}

void Log::fail(const std::string &msg) const {
	this->base();
	std::cout << "\x1B[31m" << msg << " : \x1B[1mFAILED\x1B[0m" << std::endl;
}

void Log::done(const std::string &msg) const {
	this->base();
	std::cout << "\x1B[32m" << msg << " : \x1B[1mDONE\x1B[0m" << std::endl;
}

void Log::info(const std::string &msg) const {
	this->base();
	std::cout << "\x1B[34m" << msg << "\x1B[0m" << std::endl;
}

void Log::logALError(const char* file, unsigned int line, const char* expression) const {
	/* OpenAL error : */ {
		ALenum errorCode = alGetError();

		if(errorCode != AL_NO_ERROR) {
			std::string error = "Unknown error";

			switch (errorCode) {
				case AL_INVALID_NAME:		error = "AL_INVALID_NAME"; break;
				case AL_INVALID_ENUM:		error = "AL_INVALID_ENUM"; break;
				case AL_INVALID_VALUE:		error = "AL_INVALID_VALUE"; break;
				case AL_INVALID_OPERATION:	error = "AL_INVALID_OPERATION"; break;
				case AL_OUT_OF_MEMORY:		error = "AL_OUT_OF_MEMORY"; break;
				default:					error = "Unknown error"; break;
			}

			this->fail(SSTR(error << ": " << expression << " in " << file << ":" << line));
		}
	}

	/* OpenALC error : */ {
		ALCenum errorCode = alcGetError(NULL);

		if(errorCode != ALC_NO_ERROR) {
			std::string error;

			switch (errorCode) {
				case ALC_INVALID_DEVICE:	error = "ALC_INVALID_DEVICE";break;
				case ALC_INVALID_ENUM:		error = "AL_INVALID_ENUM"; break;
				case ALC_INVALID_CONTEXT:	error = "AL_INVALID_VALUE"; break;
				case ALC_INVALID_VALUE:		error = "AL_INVALID_OPERATION"; break;
				case ALC_OUT_OF_MEMORY:		error = "AL_OUT_OF_MEMORY"; break;
				default:					error = "Unknown error"; break;
			}

			this->fail(SSTR(error << ": " << expression << " in " << file << ":" << line));
		}
	}
}
