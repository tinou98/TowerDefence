#ifndef PERFQUERY_H
#define PERFQUERY_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class PerfQuery {
public:
	PerfQuery() {	glGenQueries(1, &this->query);}
	~PerfQuery() {	glDeleteQueries(1, &this->query);}


	void start() {	 glBeginQuery(GL_PRIMITIVES_GENERATED, this->query);}
	void stop() {	 glEndQuery(GL_PRIMITIVES_GENERATED);}
	GLuint getValue(){ GLuint cpt; glGetQueryObjectuiv(this->query, GL_QUERY_RESULT, &cpt); return cpt;}

private:
	GLuint query;
};

class Locker {
public:
	Locker(PerfQuery &q) : q(q) {q.start();}
	~Locker() {q.stop();}
private:
	PerfQuery q;
};

#endif // PERFQUERY_H

