#ifndef VAO_H
#define VAO_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class VAO {
public:
	VAO()					{ glGenVertexArrays(1, &this->id); }
	~VAO()					{ glDeleteVertexArrays(1, &this->id); }
	void operator()() const	{ glBindVertexArray(this->id); }
	operator GLuint() const	{ return this->id; }

	static void clear()		{ glBindVertexArray(0); }

private:
	GLuint id = 0;
};

#endif // VAO_H
