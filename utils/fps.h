#ifndef FPS_H
#define FPS_H

#include <string>
#include "log.h"
class Fps {
public:
	Fps();

	void begin();
	double end();
	void fpsEnd();


private:
	ADD_DEFAULT_LOG

	double lastTime;
};

#endif // FPS_H
