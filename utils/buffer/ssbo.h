#ifndef SSBO_H
#define SSBO_H

#include "buffer.h"

class SSBO : public Buffer {
public:
	explicit SSBO(GLuint index);
	explicit SSBO(const SSBO &b);
	void operator ()();

	virtual void swap(SSBO &b);

	GLuint getIndex() const;

private:
	GLuint index;
};

#endif // SSBO_H
