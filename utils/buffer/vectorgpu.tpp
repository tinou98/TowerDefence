#include "vectorgpu.h"

#include <iostream>

template <typename T>
T max(const T& a, const T& b) {
	return (a > b) ? a : b;
}


template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::operator ()() {
	MemMngr::operator ()();
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, reinterpret_cast<GLint*>(&this->vao));
}

template <typename T, class MemMngr>
unsigned int VectorGPU<T, MemMngr>::getSize() { return currSize; }

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::pushBack(const T& newItem) {
	this->incSize();

	this->set(this->currSize-1, newItem);
}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::incSize(int delta) {
	if(this->currSize >= this->availibleSize)
		this->resize(max(this->currSize + delta, 2 * this->availibleSize)); // Double size

	this->currSize += delta;
}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::set(unsigned int pos, const T& val) {
	if(pos >= this->currSize)
		throw std::out_of_range(SSTR("Try write element " << pos << " but container is " << this->currSize << " lenght"));

	/* We cant use glBufferSubData */
	if((this->mapFlag & GL_DYNAMIC_STORAGE_BIT) == 0)
		this->map();

	if(this->isMapped()) {
		this->mappedPtr[pos] = val;
	} else {
		(*this)();
		glBufferSubData(this->getTarget(), sizeof(T) * pos, sizeof(T), &val);
	}
}

template <typename T, class MemMngr>
const T VectorGPU<T, MemMngr>::operator [](unsigned int pos) {
	if(pos >= this->currSize)
		throw std::out_of_range(SSTR("Try access element " << pos << " but container is " << this->currSize << " lenght"));

	if(this->isMapped())
		return this->mappedPtr[pos];
	else {
		T ret;
		(*this)();
		glGetBufferSubData(this->getTarget(), sizeof(T) * pos, sizeof(T), &ret);
		return ret;
	}
}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::resize(unsigned int newSize) {
	if(newSize == 0) return;
	if(newSize <= this->availibleSize) return;

	LOG.init(SSTR("Resize from " << this->availibleSize << " to " << newSize));

	bool wasMapped = this->isMapped();
	if(wasMapped) this->unmap();

	MemMngr tmp(*this);
	// Set size
	tmp.initStatic(sizeof(T) * newSize, this->constructFlag);
	// Copy data
	glBindBuffer(GL_COPY_READ_BUFFER, *this);
	glCopyBufferSubData(GL_COPY_READ_BUFFER, tmp.getTarget(), 0, 0, sizeof(T) * this->currSize);
	// Swap
	MemMngr::swap(tmp);

	this->availibleSize = newSize;

	LOG.done(SSTR("Resize DONE change id from " << GLuint(tmp) << " to " << MemMngr::operator GLuint()));

	if(wasMapped) this->map();
	this->updateVAO();
}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::map() {
	if(this->isMapped() || this->availibleSize == 0)
		return;

	this->mappedPtr = static_cast<T*>(MemMngr::map(0, sizeof(T) * availibleSize, this->mapFlag));
	setupNoRet(LOG, this->isMapped(), SSTR("Buffer mapping : 0->" << sizeof(T) * availibleSize),);
}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::unmap() {
	MemMngr::unmap();
	this->mappedPtr = nullptr;
	setupNoRet(LOG, !this->isMapped(), "Buffer UNmapping",);
}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::flushMap() {
	if(!this->isMapped())
		return LOG.fail("Buffer flush (Not mapped)");

	MemMngr::flushMap(0, sizeof(T) * availibleSize);
	LOG.done("Buffer flush");
}

template <typename T, class MemMngr>
bool VectorGPU<T, MemMngr>::isMapped() { return (this->mappedPtr != nullptr); }

template <typename T, class MemMngr>
T** VectorGPU<T, MemMngr>::getMappedPtr() { return &mappedPtr; }

/* As ID can change on resize, VectorVBO can auto update the VAO with new ID */
template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::setVAO(const VAO &vao) {this->vao = vao;}

template <typename T, class MemMngr>
void VectorGPU<T, MemMngr>::updateVAO() {
	glBindVertexArray(this->vao);
	(*this)();
}
