#include "ssbo.h"

SSBO::SSBO(GLuint index) : Buffer(GL_SHADER_STORAGE_BUFFER), index(index) {}
SSBO::SSBO(const SSBO &b) : SSBO(b.getIndex()) {}

void SSBO::swap(SSBO &b) {
	std::swap(this->index, b.index);
	Buffer::swap(b);
}

GLuint SSBO::getIndex() const {
	return this->index;
}

void SSBO::operator ()() {
	Buffer::operator ()();
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, this->index, *this);
}
