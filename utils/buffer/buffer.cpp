#include "buffer.h"

Buffer::Buffer(GLenum target) : target(target) {
	glGenBuffers(1, &this->id);
}

Buffer::Buffer(const Buffer &b) : Buffer(b.getTarget()) {}

Buffer::~Buffer() {
	glDeleteBuffers(1, &this->id);
}

#include <algorithm>
void Buffer::swap(Buffer &b) {
	std::swap(this->target, b.target);
	std::swap(this->id, b.id);
}

Buffer::operator GLuint() const	{ return this->id;}

void Buffer::init(GLenum usage, int size, void *data) {
	(*this)();
	glBufferData(target, size, data, usage);
}

#include <chrono>
#include <thread>
#include <iostream>
void Buffer::initStatic(int size, GLbitfield flags, void *data) {
	(*this)();
	glBufferStorage(target, size, data, flags);
}

void Buffer::operator ()() const	{ glBindBuffer(target, this->id); }

GLenum Buffer::getTarget() const {
	return this->target;
}

void Buffer::setTarget(GLenum target) {
	this->target = target;
}

void* Buffer::map(GLintptr from, GLsizeiptr to, GLbitfield mapFlag) const {
	(*this)();
	return glMapBufferRange(this->target, from, to, mapFlag);
}

void Buffer::flushMap(GLintptr offset, GLsizeiptr lenght) const {
	(*this)();
	glFlushMappedBufferRange(this->target, offset, lenght);
}

void Buffer::unmap() const {
	(*this)();
	glUnmapBuffer(this->target);
}
