#ifndef BUFFER_H
#define BUFFER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>

class Buffer {
public:
	explicit Buffer(GLenum target);
	explicit Buffer(const Buffer &b);
	virtual ~Buffer();

	virtual void swap(Buffer &b);


	void init(GLenum usage, int size, void *data = nullptr);
	void initStatic(int size, GLbitfield flags, void *data = nullptr);

	void* map(GLintptr from, GLsizeiptr to, GLbitfield mapFlag) const;
	void flushMap(GLintptr offset, GLsizeiptr lenght) const;
	void unmap() const;

	virtual void operator ()() const;
	operator GLuint() const;
	GLenum getTarget() const;
	void setTarget(GLenum target);

private:
	GLuint id = 0;
	GLenum target;
};

#endif // BUFFER_H
