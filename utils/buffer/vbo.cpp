#include "vbo.h"

VBO::VBO() : Buffer(GL_ARRAY_BUFFER) {}
VBO::VBO(const VBO &vbo) : Buffer(GL_ARRAY_BUFFER), chunkList(vbo.chunkList) {}

void VBO::swap(VBO &b) {
	std::swap(this->chunkList, b.chunkList);

	Buffer::swap(b);
}

void VBO::addItem(vboConfig el) {this->chunkList.push_back(el);}
void VBO::setItems(std::vector<vboConfig> list) {
	this->chunkList.clear();
	this->chunkList = list;
}

void VBO::addAndSetItems(std::vector<vboEntry> list) {
	this->chunkList.clear();

	int totSize = 0;
	for(vboEntry elem : list)
		totSize += std::get<1>(elem);

	this->init(GL_STATIC_DRAW, totSize, nullptr);

	GLintptr offset = 0;
	for(vboEntry elem : list) {
		this->chunkList.push_back(std::get<2>(elem));
		std::get<2>(elem).setOffset(offset);
		chunkList.push_back(std::get<2>(elem));

		glBufferSubData(this->getTarget(), offset, std::get<1>(elem), std::get<0>(elem));
		std::get<2>(elem)();

		offset += std::get<1>(elem);
	}
}

void VBO::operator ()() const {
	Buffer::operator ()();
	this->enable();
}

void VBO::enable() const {
	for(const auto& conf : chunkList) {
		conf();
	}
}

void VBO::disable() const {
	for(const auto& conf : chunkList)
		glDisableVertexAttribArray(conf.index);
}
