#ifndef VBO_H
#define VBO_H

#include "buffer.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>

#define CAST_TO_PTR(x) ((GLvoid*)(long)x)

class vboConfig {
public:
	enum class internalType {
		Float,
		Int,
		Double,
	};

	vboConfig() {}

	vboConfig(GLuint index, GLint size, GLenum type, GLboolean normalised, GLsizei stride, unsigned int offset = 0, unsigned int divisor = 0,  internalType outType = internalType::Float)
		: index(index), size(size), type(type), normalised(normalised), stride(stride), pointer(CAST_TO_PTR(offset)), divisor(divisor), outType(outType) {}

	GLuint index;
	GLint size;
	GLenum type;
	GLboolean normalised;
	GLsizei stride;
	GLvoid *pointer; // Offset
	unsigned int divisor;
	internalType outType;

	void setOffset(unsigned int offset) {pointer = CAST_TO_PTR(offset);}

	void operator()() const {
		switch (this->outType) {
			case internalType::Float:
				glVertexAttribPointer(this->index, this->size, this->type, this->normalised, this->stride, this->pointer);
				break;

			case internalType::Int:
				glVertexAttribIPointer(this->index, this->size, this->type, this->stride, this->pointer);
				break;

			case internalType::Double:
				glVertexAttribLPointer(this->index, this->size, this->type, this->stride, this->pointer);
				break;
		}

		glEnableVertexAttribArray(this->index);
		glVertexAttribDivisor(this->index, this->divisor);
	}
};

typedef std::tuple<GLvoid*, GLsizeiptr, vboConfig> vboEntry;

#include <tuple>
template<typename T>
vboEntry makeVBOrow(const std::vector<T> &arr, vboConfig c) {
	vboEntry ret((GLvoid*)(&arr.at(0)), (GLsizeiptr)(arr.size() * sizeof(T)), c);
	return ret;
}

class VBO : public Buffer {
public:
	VBO();
	VBO(const VBO &vbo);

	virtual void swap(VBO &b);

	void addAndSetItems(std::vector<vboEntry> list);

	void addItem(vboConfig el);
	void setItems(std::vector<vboConfig> list);

	void operator ()() const override;
	void enable() const;
	void disable() const;

private:
	std::vector<vboConfig> chunkList;
};

#endif // VBO_H
