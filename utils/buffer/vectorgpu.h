#ifndef VECTORVBO_H
#define VECTORVBO_H

#include <utils/log.h>
#include <utils/vao.h>

template <typename T, class MemMngr>
class VectorGPU : public MemMngr {
public:
	template<typename... ConstructArgs>
	explicit VectorGPU(ConstructArgs... args) : MemMngr(args...) {}

	virtual void operator ()();

	unsigned int getSize();

	void pushBack(const T& newItem);
	void incSize(int delta = 1);

	void set(unsigned int pos, const T& val);

	const T operator [](unsigned int pos);

	void resize(unsigned int newSize);

	void map();
	void flushMap();
	void unmap();
	bool isMapped();
	T** getMappedPtr();

	/* As ID can change on resize, VectorVBO can auto update the VAO with new ID */
	void setVAO(const VAO &vao);
	void updateVAO();

	GLenum constructFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
	GLenum mapFlag = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
protected:
	unsigned int currSize = 0;
	unsigned int availibleSize = 0;

private:
	ADD_DEFAULT_LOG

	T* mappedPtr = nullptr; // Pointer to mapped memory (or nullptr)

	GLenum usageHint;

	using Buffer::init;
	using Buffer::initStatic;
	using Buffer::map;
	using Buffer::unmap;

	GLuint vao = 0;
};

template <typename T, class MemMngr> const Log VectorGPU<T, MemMngr>::log("VectorGPU");

#include "vectorgpu.tpp"

#endif // VECTORVBO_H
