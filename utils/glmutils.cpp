#include "glmutils.h"

#include <iostream>
#include <iomanip>

void printMatrix(const glm::mat4 &m) {
	std::cout << "############################################################" << std::endl;

	for(int i = 0; i < 4; i++) {
		for(int j = 0; j < 4; j++)
			std::cout << std::setw(15) << m[j][i];

		std::cout << std::endl;
	}

	std::cout << "############################################################" << std::endl;
}

vec1::vec1(float x) : x(x) {}
vec2::vec2(float x, float y) : x(x), y(y) {}
vec3::vec3(float x, float y, float z) : x(x), y(y), z(z) {}
vec4::vec4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

bool operator ==(vec1 lhs, vec1 rhs) {return (lhs.x == rhs.x);}
bool operator ==(vec2 lhs, vec2 rhs) {return (lhs.y == rhs.y) && (lhs.x == rhs.x);}
bool operator ==(vec3 lhs, vec3 rhs) {return (lhs.z == rhs.z) && (lhs.y == rhs.y) && (lhs.x == rhs.x);}
bool operator ==(vec4 lhs, vec4 rhs) {return (lhs.w == rhs.w) && (lhs.z == rhs.z) && (lhs.y == rhs.y) && (lhs.x == rhs.x);}
