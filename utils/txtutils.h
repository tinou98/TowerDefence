#ifndef TXTUTILS_H
#define TXTUTILS_H

#include <string>

std::string &ltrim(std::string &s);
std::string &rtrim(std::string &s);
std::string &trim(std::string &s);

std::string trimCopy(std::string s);


#endif // TXTUTILS_H
