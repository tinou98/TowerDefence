#include "fps.h"

#include <GLFW/glfw3.h>

SETUP_LOG(Fps, "FPS")

Fps::Fps() {this->begin();}

void Fps::begin() { this->lastTime = glfwGetTime(); }

#include <iostream>
#include <iomanip>
double Fps::end() {
	double delta = glfwGetTime() - this->lastTime;

	return delta;
}

void Fps::fpsEnd() {
	double delta = this->end();
	LOG.info(SSTR(std::setw(3) << (int)(delta*1000) << "ms" << std::setw(3) << "\t" << (int)(1.0/delta) << " fps"));
}
