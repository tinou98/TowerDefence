#ifndef LOG_H
#define LOG_H

#include <string>
#include <stdexcept>

//#define setupNoRet(log, test, txt, failCmd)		{std::string str(txt);log.init(str); if(!(test)) {log.fail(str); failCmd;} else log.done(str);}
#define setupNoRet(log, test, txt, failCmd)		{std::string str(txt); if(!(test)) {log.fail(str); failCmd;} else log.done(str);}
//#define setupNoRet(log, test, txt, failCmd)		{if(!(test)) failCmd; }
#define setupThrow(log, test, txt)				setupNoRet(log, test, txt, throw std::runtime_error(txt))
#define setup(log, test, txt)					setupNoRet(log, test, txt, return;)

#include <sstream>
#define SSTR( x ) static_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()


#define ALexec(log, expr) {log.logALError(__FILE__, __LINE__, "/!\\ ERROR BEFORE"); expr; log.logALError(__FILE__, __LINE__, #expr);}

#define ADD_DEFAULT_LOG static const Log log;
#define SETUP_LOG(className, str)	const Log className::log(str);
#define LOG this->log

class Log
{
public:
	explicit Log(const std::string &component);
	void init(const std::string &msg) const;
	void fail(const std::string &msg) const;
	void done(const std::string &msg) const;
	void info(const std::string &msg) const;

	void logALError(const char* file, unsigned int line, const char* expression) const;

private:
	void base() const;
	std::string component;
};

#endif // LOG_H
