#include "rendertotexture.h"

SETUP_LOG(renderToTexture, "RENDERTOTEXTURE")

#include <string>
renderToTexture::renderToTexture(int width, int height, bool depthTest, GLenum format) : width(width), height(height), depthTest(depthTest), format(format) {
	this->gen();

	setupThrow(LOG, glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE, "Setup done");
}


renderToTexture::~renderToTexture() {
	this->del();
}

void renderToTexture::gen() {
	if(this->height == 0 || this->width == 0) return;
	this->del();

	glGenFramebuffers(1, &this->frameBuffID);
	glBindFramebuffer(GL_FRAMEBUFFER, this->frameBuffID); {
		/* Render texture */ {
			glGenTextures(1, &this->renderedTextureID);
			glBindTexture(GL_TEXTURE_2D, this->renderedTextureID);

			// Clear texture
			glTexImage2D(GL_TEXTURE_2D, 0, this->format, this->width, this->height, 0, this->format, GL_UNSIGNED_BYTE, 0);


			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->renderedTextureID, 0);
		}

		/* Depth buffer */
		if(depthTest) {
			glGenRenderbuffers(1, &this->depthRenderID);
			glBindRenderbuffer(GL_RENDERBUFFER, this->depthRenderID);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderID);
		}

	} glBindFramebuffer(GL_FRAMEBUFFER, 0);

	LOG.done("Objects (re)generated");
}

void renderToTexture::del() {
	glDeleteRenderbuffers(1, &this->depthRenderID); this->depthRenderID = 0;
	glDeleteTextures(1, &this->renderedTextureID); this->renderedTextureID = 0;
	glDeleteFramebuffers(1, &this->frameBuffID); this->frameBuffID = 0;
}

void renderToTexture::resize(int width, int height) {
	this->height = height;
	this->width = width;

	this->gen();
}

void renderToTexture::bindToDraw() {
	glBindFramebuffer(GL_FRAMEBUFFER, this->frameBuffID);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glViewport(0, 0, this->width, this->height);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void renderToTexture::bindTexture() {
	glBindTexture(GL_TEXTURE_2D, this->renderedTextureID);
}


