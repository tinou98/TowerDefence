#ifndef GLMUTILS_H
#define GLMUTILS_H

#include <glm/glm.hpp>

void printMatrix(const glm::mat4 &m);

struct vec1 {
	explicit vec1(float x = 0);
	float x;
};

struct vec2 {
	explicit vec2(float x = 0, float y = 0);
	float x, y;
};

struct vec3 {
	explicit vec3(float x = 0, float y = 0, float z = 0);
	float x, y, z;
};

struct vec4 {
	explicit vec4(float x = 0, float y = 0, float z = 0, float w = 0);
	float x, y, z, w;
};

bool operator ==(vec1 lhs, vec1 rhs);
bool operator ==(vec2 lhs, vec2 rhs);
bool operator ==(vec3 lhs, vec3 rhs);
bool operator ==(vec4 lhs, vec4 rhs);

#endif // GLMUTILS_H
