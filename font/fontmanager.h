#ifndef FONTMANAGER_H
#define FONTMANAGER_H

#include <utils/log.h>

#include <string>
#include <unordered_map>

#include <ft2build.h>
#include FT_FREETYPE_H

class Font;

class FontManager {
public:
	FontManager();
	~FontManager();

	Font* getFont(std::string s, int sizePt = 32);
private:
	ADD_DEFAULT_LOG
	FT_Library library;

	std::unordered_map<std::string, Font*> alreadyBuilt;

	double dpiH, dpiV;
};

#endif // FONTMANAGER_H
