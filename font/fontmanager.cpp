#include "fontmanager.h"

#include "font.h"

SETUP_LOG(FontManager, "FONT-MNGR")

FontManager::FontManager() {
	LOG.init("Loading FontManager");

	FT_Error error = FT_Init_FreeType(&this->library);
	setupThrow(LOG, !error, "Initialisation of FreeType");

	auto monitor = glfwGetPrimaryMonitor();
	int widthMM, heightMM;
	glfwGetMonitorPhysicalSize(monitor, &widthMM, &heightMM);
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	this->dpiH = mode->width / (widthMM / 25.4);
	this->dpiV = mode->height / (heightMM / 25.4);

	LOG.info(SSTR("DPI : " << dpiH << "x" << dpiV));
}

FontManager::~FontManager() {
	for(const auto& f : alreadyBuilt) {
		FT_Done_Face(f.second->face);
		delete f.second;
	}

	setup(LOG, !FT_Done_FreeType(this->library), "Deleting FontManager")
}

Font* FontManager::getFont(std::string s, int sizePt) {
	try {
		return this->alreadyBuilt.at(s);
	} catch(std::out_of_range) {
		FT_Face face;

		FT_Error error = FT_New_Face(this->library, s.c_str(), 0, &face);
		if (error == FT_Err_Unknown_File_Format) {
			LOG.fail(SSTR("Unable to open " << s << " : UNKNOW FILE FORMAT"));

			throw std::runtime_error(SSTR("Unable to open " << s << " : UNKNOW FILE FORMAT"));
		} else if (error) {
			LOG.fail(SSTR("Can't open file " << s));

			throw std::runtime_error(SSTR("Can't open file " << s));
		}

		setupNoRet(LOG, !FT_Set_Char_Size(face, 0, sizePt*64, dpiH, dpiV), "Set size point",);

		Font *f = new Font(face);
		this->alreadyBuilt.insert({s, f});
		return f;
	}
}
