#include "font.h"

SETUP_LOG(Font, "FONT")

Font::Font(FT_Face face) : face(face) {
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
}

Char Font::getGlyph(FT_ULong c) {
	try {
		return this->lst.at(c);
	} catch(std::out_of_range) {
		LOG.info(SSTR("Generate glyph : " << (char)c));
//		FT_Uint idx = FT_Get_Char_Index(this->face, c);
//		FT_Load_Glyph(this->face, idx, FT_LOAD_RENDER);


		setupNoRet(LOG, !FT_Load_Char(face, c, FT_LOAD_RENDER), SSTR("Load & Render char " << (char)c), /*return getGlyph(0)*/);

		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer );

		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Now store character for later use
		Char character = {
			texture,
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x
		};
		this->lst.insert({c, character});

		return character;
	}
}
