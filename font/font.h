#ifndef FONT_H
#define FONT_H

#include <glm/glm.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <utils/log.h>
#include "fontmanager.h"

#include <unordered_map>

#include <ft2build.h>
#include FT_FREETYPE_H

struct Char {
	GLuint textureID;
	glm::ivec2 size;
	glm::ivec2 bearing;
	FT_Pos advance;
};

class Font {
	friend Font* FontManager::getFont(std::string, int);
	friend FontManager::~FontManager();
public:
	Char getGlyph(FT_ULong c);

private:
	ADD_DEFAULT_LOG

	explicit Font(FT_Face face);

	FT_Face face;
	std::unordered_map<FT_ULong, Char> lst;
};

#endif // FONT_H
