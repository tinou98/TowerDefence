#version 330 core

in vec2 uv;
layout(location = 0) out vec3 finalColor;

uniform sampler2D tex;


void main() {
	finalColor = texture2D(tex, uv).rrr;
}
