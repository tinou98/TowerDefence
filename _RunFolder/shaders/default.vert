#version 430 core
#extension GL_ARB_shader_storage_buffer_object : require
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_shader_draw_parameters : require

/* Layout */
layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in uvec4 bonesId;
layout(location = 3) in vec4 bonesWeight;
layout(location = 4) in int id;

uniform mat4 VP;

/* SSBO */
layout(std430, binding = 0) buffer perMesh {
	struct {
		int useless;
	} meshs[];
};
layout(std430, binding = 1) buffer perObject {
	struct {
		mat4 mat;

/*		uint8_t from1;
		uint8_t from2;
		float fromPrct;

		uint8_t to;
		float prct;*/
	} objects[];
};


/* Data to Fragment Shader */
out gl_PerVertex {
	vec4 gl_Position;
};

out block {
	vec3 UV;
} Out;



void main() {
	gl_Position = (VP * objects[id].mat) * vec4(vertexPos, 1);
	
	Out.UV = vec3(vertexUV, 0);
}
