#version 410 core

in block {
	vec2 UV;
	vec3 color;
} In;

layout(location = 0) out vec3 finalColor;

uniform sampler2D myTextureSampler;

void main(){
	finalColor = In.color;
	
	if(textureSize(myTextureSampler, 0).x > 1)
		finalColor *= texture(myTextureSampler, In.UV).rgb;
}