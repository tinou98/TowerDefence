#version 450 core

in block {
	float height;
};

layout(location = 0) out vec3 finalColor;

void main() {
	if(height > 0.8)
		finalColor = vec3(1, 1, 1);
	else if(height > 0.5)
		finalColor = vec3(0, 1, 0);
	else if(height > 0.2)
		finalColor = vec3(0.1, 0.1, 0.1);
	else
		finalColor = vec3(0, 0, 1);
}
