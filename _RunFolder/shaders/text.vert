#version 330 core

layout(location = 0) in vec4 vertex_uv;
out vec2 uv;

uniform mat4 resize;

void main() {
  gl_Position = resize * vec4(vertex_uv.xy, 0, 1);
  uv = vertex_uv.zw;
}