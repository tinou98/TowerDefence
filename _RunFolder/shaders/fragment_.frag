#version 330 core

in vec2 UV;
layout(location = 0) out vec4 finalColor;

uniform sampler2D myTextureSampler; 
uniform vec3 color;

void main(){	
	finalColor = vec4(color, 1 - texture(myTextureSampler, UV).r);
}