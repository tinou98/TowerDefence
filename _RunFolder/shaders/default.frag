#version 410 core

/* INPUT */

/* From Vertex */
in block {
	vec3 UV;
} In;

/* From App */
//uniform sampler2DArray myTextureSampler;

/* Output -> FrameBuffer */
layout(location = 0) out vec4 finalColor;



void main(){
//	finalColor = texture(myTextureSampler, In.UV);
	finalColor = vec4(1, 0, 0, 1);
}
