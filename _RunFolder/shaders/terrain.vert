#version 450 core
#extension GL_EXT_gpu_shader4 : require

layout(std140, binding=1) uniform Params {
	vec2 size;
	ivec2 divisionNum;
	mat4 mat;
};


layout(location=0) in vec3 vertex;

out gl_PerVertex {
    vec4 gl_Position;
};

out block {
     vec2 texCoord;
};

void main() {
    // position patch in 2D grid based on instance ID
    ivec2 pos = ivec2(gl_InstanceID % divisionNum.x, gl_InstanceID / divisionNum.x);

    texCoord = vec2(pos) / vec2(divisionNum);
	
//    gl_Position = vec4(vertex + vec3(size * (pos/divisionNum - vec2(1/2, 1/2)), 0), 1.0f);
	gl_Position = vec4(vertex + vec3(-size/2 + size*vec2(pos)/vec2(divisionNum), 0), 1.0f);
}