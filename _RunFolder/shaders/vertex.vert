#version 430 core
#extension GL_ARB_shader_storage_buffer_object : require

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in int id;
uniform mat4 VP;
uniform vec3 color;

layout(std430, binding = 0) buffer perObject {
	mat4 M[];
};

out block {
	vec2 UV;
	vec3 color;
} Out;

out gl_PerVertex {
	vec4 gl_Position;
};



void main() {
	gl_Position = (VP * M[id]) * vec4(vertexPos, 1);
	
	Out.UV = vertexUV;
	Out.color = color;
}