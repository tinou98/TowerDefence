#ifndef OPENGLCTX_H
#define OPENGLCTX_H

#include <utils/log.h>
#include <camera/camera.h>
#include <entity/group.h>

#include <iostream>
#include <stdlib.h>
#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class openGlCtx {
public:
	openGlCtx(int glCtxVMaj = 3, int glCtxVMin = 3);
	~openGlCtx();


	operator GLFWwindow*() { return this->window; }
	void operator ()();

	Camera *cam;
	Group group;

	int height = 1080,
		width = 1920;
	float fov = 45.0;
private:
	// No Copy
	openGlCtx(const openGlCtx &other) = delete;

	GLFWwindow* window;
	Log log;
	Log *openGLLog;

};

#endif // OPENGLCTX_H
