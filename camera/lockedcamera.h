#ifndef LOCKEDCAMERA_H
#define LOCKEDCAMERA_H

#include "camera.h"

class LockedCamera : public Camera {
public:
	LockedCamera(GLFWwindow* win, glm::vec3 pos, glm::vec3 center, glm::vec3 upVect = glm::vec3(0, 0, 1));

	glm::mat4 compute(double dx, double dy) override;
private:
	glm::vec3 pos, center, upVect;
};

#endif // LOCKEDCAMERA_H
