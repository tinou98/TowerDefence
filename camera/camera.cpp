#include "camera.h"

Camera::Camera(GLFWwindow* win) : win(win) {
}

glm::mat4 Camera::update(double x, double y) {
    glm::mat4 ret = this->compute(x - oldX, y - oldY);

    this->oldY = y;
    this->oldX = x;

    return ret;
}
