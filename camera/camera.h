#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Camera {
public:
    explicit Camera(GLFWwindow* win);
    virtual ~Camera() = 0;

	virtual glm::mat4 compute(double dx, double dy) = 0;

	glm::mat4 update(double x, double y) {
		glm::mat4 ret = this->compute(x - oldX, y - oldY);

		this->oldY = y;
		this->oldX = x;

		return ret;
	}

protected:
	GLFWwindow* win;

private:
	double	oldX = 0,
			oldY = 0;
};

#endif // CAMERA_H
