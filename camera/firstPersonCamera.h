#ifndef FIRSTPERSONCAMERA_H
#define FIRSTPERSONCAMERA_H

#include "camera.h"

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

class FirstPersonCamera : public Camera {
public:
	FirstPersonCamera(GLFWwindow* win, glm::vec3 pos = glm::vec3(0, 0, 0), glm::vec3 upVect = glm::vec3(0, 0, 1));

	glm::vec3 getPos();
	glm::vec3 getDir();
	glm::mat4 compute(double dx, double dy);

private:
	glm::vec3 pos, upVect;
	double theta = 0.0, phi = 0.0;
};

#endif // FIRSTPERSONCAMERA_H
