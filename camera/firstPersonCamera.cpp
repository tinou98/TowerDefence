#include "firstPersonCamera.h"

FirstPersonCamera::FirstPersonCamera(GLFWwindow *win, glm::vec3 pos, glm::vec3 upVect) : Camera(win), pos(pos), upVect(glm::normalize(upVect)) {
	this->theta = 0;
	this->phi = 0;
}

glm::vec3 FirstPersonCamera::getPos() { return this->pos; }

glm::vec3 FirstPersonCamera::getDir() {
	return glm::vec3(
		cos(this->theta)*cos(this->phi),
		sin(this->theta)*cos(this->phi),
		sin(this->phi)
	);
}


#include <glm/gtc/matrix_transform.hpp>
glm::mat4 FirstPersonCamera::compute(double dx, double dy) {
	double speedMoove = 0.05,
			speedRotV = 0.005,
			speedRotH = 0.005;

	if(glfwGetKey(this->win, GLFW_KEY_LEFT_SHIFT))
		speedMoove = 10;

	if(glfwGetKey(this->win, GLFW_KEY_LEFT_CONTROL)) {
		speedMoove *= 4;
	}

	if(glfwGetKey(this->win, GLFW_KEY_W)) this->pos += this->getDir() *glm::vec3(speedMoove);
	if(glfwGetKey(this->win, GLFW_KEY_S)) this->pos -= this->getDir() *glm::vec3(speedMoove);


	if(glfwGetKey(this->win, GLFW_KEY_Q)) this->pos += this->upVect *glm::vec3(speedMoove);
	if(glfwGetKey(this->win, GLFW_KEY_E)) this->pos -= this->upVect *glm::vec3(speedMoove);

	if(glfwGetKey(this->win, GLFW_KEY_D)) this->pos += glm::normalize(glm::cross(this->getDir(), this->upVect)) *glm::vec3(speedMoove);
	if(glfwGetKey(this->win, GLFW_KEY_A)) this->pos -= glm::normalize(glm::cross(this->getDir(), this->upVect)) *glm::vec3(speedMoove);

	this->theta -= dx *speedRotH;
	this->phi -= dy *speedRotV;

	if(this->phi > 1.5707) this->phi = 3.14/2;
	if(this->phi < -1.5707) this->phi = -3.14/2;

	this->theta = fmod(this->theta, 2*3.1415);

	return glm::lookAt(this->pos, this->pos + this->getDir(), this->upVect);
}
