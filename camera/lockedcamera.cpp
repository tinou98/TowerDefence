#include "lockedcamera.h"

LockedCamera::LockedCamera(GLFWwindow *win, glm::vec3 pos, glm::vec3 center, glm::vec3 upVect) : Camera(win), pos(pos), center(center), upVect(glm::normalize(upVect)) {}

#include <glm/gtc/matrix_transform.hpp>
glm::mat4 LockedCamera::compute(double dx, double dy) {
	return glm::lookAt(this->pos, this->center, this->upVect);
}
