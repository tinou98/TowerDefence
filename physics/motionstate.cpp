#include "motionstate.h"

#include <glm/gtc/type_ptr.hpp>

using namespace Physics;


MotionState::MotionState(const btTransform& position,const btTransform& centerOfMassOffset) : pos(position), centerOfMassOffset(centerOfMassOffset) {}

MotionState::~MotionState() {}

void MotionState::getWorldTransform(btTransform &worldTrans) const {
	worldTrans = this->pos * this->centerOfMassOffset.inverse();
}

void MotionState::setWorldTransform(const btTransform &worldTrans) {
	this->pos = worldTrans * this->centerOfMassOffset;

	if(this->updateCallback)
		this->updateCallback(this->toMat4());
}

glm::mat4 MotionState::toMat4(){
	glm::mat4 ret;

	(this->pos * this->centerOfMassOffset.inverse()).getOpenGLMatrix(glm::value_ptr(ret));

	return ret;
}
