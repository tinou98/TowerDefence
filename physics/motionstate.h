#ifndef MOTIONSTATE_H
#define MOTIONSTATE_H

#include <functional>

#include <LinearMath/btMotionState.h>
#include <glm/glm.hpp>

namespace Physics {

class MotionState : public btMotionState
{
public:
	MotionState(const btTransform& position,const btTransform& centerOfMassOffset = btTransform::getIdentity());

	virtual ~MotionState() override;
	virtual void getWorldTransform(btTransform &worldTrans) const override;
	virtual void setWorldTransform(const btTransform &worldTrans) override;

	glm::mat4 toMat4();
	std::function<void(glm::mat4)> updateCallback;

private:
	btTransform pos;
	btTransform centerOfMassOffset;
};

}

#endif // MOTIONSTATE_H
