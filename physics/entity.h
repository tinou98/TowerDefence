#ifndef ENTITY_H
#define ENTITY_H

#include "motionstate.h"

#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <BulletCollision/CollisionShapes/btCollisionShape.h>

namespace Physics {

class CollisionShape;
class DynamicsWorld;

class Entity {
public:
	friend class DynamicsWorld;

	Entity(btTransform pos, const CollisionShape &shape, btTransform offset = btTransform::getIdentity());
	virtual ~Entity();

//	operator btRigidBody *();

private:
	btRigidBody *body;

protected:
	MotionState state;
};

}

#endif // ENTITY_H
