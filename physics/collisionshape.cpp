#include "collisionshape.h"

using namespace Physics;

CollisionShape::CollisionShape(btCollisionShape *newShape) {
	this->shape = newShape;
}

std::ostream &operator <<(std::ostream &os, const Physics::CollisionType &id) {
	switch(id) {
//		case CollisionType::Heightfield:
//			return os << "CollisionType::Heightfield";
		case CollisionType::BvhTriangleMeshShape:
			return os << "CollisionType::BvhTriangleMeshShape";
		case CollisionType::BoxShape:
			return os << "CollisionType::BoxShape";
		case CollisionType::SphereShape:
			return os << "CollisionType::SphereShape";
		case CollisionType::CapsuleShape:
			return os << "CollisionType::CapsuleShape";
		case CollisionType::CylinderShape:
			return os << "CollisionType::CylinderShape";
		case CollisionType::ConeShape:
			return os << "CollisionType::ConeShape";
		case CollisionType::ConvexHullShape:
			return os << "CollisionType::ConvexHullShape";
		case CollisionType::CompoundShape:
			return os << "CollisionType::CompoundShape";
		case CollisionType::UnknowType:
			return os << "CollisionType::UnknowType";
		default:
			return os << "CollisionType::????";
	}
}

CollisionShape::CollisionShape(CollisionType id, ColisionShapeParam param) {
	this->shape = nullptr;

	switch(id) {
		/*case CollisionType::Heightfield:
			this->shape = new btHeightfield(
							  param.Heightfield.heightStickWidth,
							  param.Heightfield.heightStickLength,
							  param.Heightfield.heightfieldData,
							  param.Heightfield.heightScale,
							  param.Heightfield.minHeight,
							  param.Heightfield.maxHeight,
							  param.Heightfield.upAxis,
							  param.Heightfield.heightDataType,
							  param.Heightfield.flipQuadEdges);
			break;*/
		/*case CollisionType::BvhTriangleMeshShape:
			this->shape = new btBvhTriangleMeshShape(param.BvhTriangleMeshShape);
			break;*/
		case CollisionType::BoxShape:
			this->shape = new btBoxShape(param.BoxShape.boxHalfExtents);
			break;
		case CollisionType::SphereShape:
			this->shape = new btSphereShape(param.SphereShape.radius);
			break;
		case CollisionType::CapsuleShape:
			this->shape = new btCapsuleShape(param.CapsuleShape.radius, param.CapsuleShape.height);
			break;
		case CollisionType::CylinderShape:
			this->shape = new btCylinderShape(param.CylinderShape.halfExtents);
			break;
		case CollisionType::ConeShape:
			this->shape = new btConeShape(param.ConeShape.radius, param.ConeShape.height);
			break;
		/*case CollisionType::ConvexHullShape:
			this->shape = new btConvexHullShape(param.ConvexHullShape);
			break;*/
		/*case CollisionType::CompoundShape:
			this->shape = new btCompoundShape(param.CompoundShape);
			break;*/

		default:
			std::cout << "Unknow Type : " << id << std::endl;
		case CollisionType::UnknowType:
			this->shape = nullptr;
	}
}

CollisionShape::~CollisionShape() {
	delete this->shape;
}

btVector3 CollisionShape::calculateLocalInertia() const {
	btVector3 inertia(0, 0, 0);

	if(this->mass != 0)
		this->shape->calculateLocalInertia(this->mass, inertia);

	return inertia;
}

btRigidBody::btRigidBodyConstructionInfo CollisionShape::getRigidBodyCreationInfo(MotionState *state) const {
	return btRigidBody::btRigidBodyConstructionInfo(this->mass, state, this->shape, this->calculateLocalInertia());
}
