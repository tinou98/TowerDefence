#ifndef DEBUG_H
#define DEBUG_H

#include <LinearMath/btIDebugDraw.h>
#include <entity/3D/entity3d.h>
#include <entity/3D/line.h>

namespace Physics {

class Debug : public btIDebugDraw, public Entity3D
{
public:
	Debug();

	void operator ()(glm::mat4 mat) override;

	virtual void drawLine(const btVector3& from,const btVector3& to,const btVector3& color) override;
	virtual void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &fromColor, const btVector3 &toColor) override;
	virtual void drawContactPoint(const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color) override;
	virtual void reportErrorWarning(const char *warningString) override;
	virtual void draw3dText(const btVector3 &location, const char *textString) override;
	virtual void setDebugMode(int debugMode) override;
	virtual int getDebugMode() const override;

	virtual void flushLines() override;

private:
	glm::mat4 mat;
	DebugDrawModes mode;

	Line lineDrawer;
};

}

#endif // DEBUG_H
