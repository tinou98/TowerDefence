#include "debug.h"

#include <iostream>
#define WIP std::cout << "WIP : " << __PRETTY_FUNCTION__ << "@" << __FILE__ << ":" << __LINE__ << std::endl;

using namespace Physics;

#define LINEMAX 200

Debug::Debug() : lineDrawer(LINEMAX) {
	this->setDebugMode(DBG_DrawWireframe | DBG_DrawAabb | DBG_DrawNormals | DBG_DrawFrames | DBG_FastWireframe);
	this->setDebugMode(DBG_DrawWireframe
					   | DBG_DrawAabb
					   | DBG_DrawFeaturesText
					   | DBG_DrawContactPoints
//					   | DBG_NoDeactivation
//					   | DBG_NoHelpText
					   | DBG_DrawText
					   | DBG_ProfileTimings
					   | DBG_EnableSatComparison
//					   | DBG_DisableBulletLCP
					   | DBG_EnableCCD
					   | DBG_DrawConstraints
					   | DBG_DrawConstraintLimits
					   | DBG_FastWireframe
					   | DBG_DrawNormals
					   | DBG_DrawFrames
					   );
}

void Debug::operator ()(glm::mat4 mat) {
	this->mat = mat;
}
void Debug::drawLine(const btVector3& from,const btVector3& to,const btVector3& color) {
	this->drawLine(from, to, color, color);
}

void Debug::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &fromColor, const btVector3 &toColor) {
	if(lineDrawer.pushBack(from, to, fromColor, toColor) >= LINEMAX)
		this->flushLines();
}

void Debug::drawContactPoint(const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color) {
	btVector3 to = PointOnB + (normalOnB * distance);
	this->drawLine(PointOnB, to, color);
}

void Debug::reportErrorWarning(const char *warningString) {
	WIP;
	std::cout << "Error message : " << warningString << std::endl;
}

void Debug::draw3dText(const btVector3 &location, const char *textString) {
	WIP;
	std::cout << "Text @" << location << " " << textString << std::endl;
}

void Debug::setDebugMode(int debugMode) {
	this->mode = static_cast<DebugDrawModes>(debugMode);
}

int Debug::getDebugMode() const {
	return static_cast<int>(this->mode);
}

void Debug::flushLines() {
	lineDrawer(mat);
}

