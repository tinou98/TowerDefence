#ifndef DYNAMICSWORLD_H
#define DYNAMICSWORLD_H

#include <btBulletDynamicsCommon.h>
#include <list>
#include <utils/fps.h>

#include "debug.h"

namespace Physics {

class Entity;

class DynamicsWorld {
	friend class Entity;
public:
	DynamicsWorld();
	~DynamicsWorld();

	void operator ()();
	void operator ()(glm::mat4 mat);

	void addRigidBody(const Entity &rb);
	void removeRigidBody(const Entity &rb);

private:
	DynamicsWorld(const DynamicsWorld &other) = delete;

	btDiscreteDynamicsWorld *dynamicsWorld;
	btConstraintSolver *solver;
	btDispatcher *dispatcher;
	btCollisionConfiguration *collisionConfiguration;
	btBroadphaseInterface *broadphase;

	// OpenGL debug
	Debug debug;

	// List of Entity
	std::list<btRigidBody *> entityList;

	// Get delaTime
	Fps f;
};

}

#endif // DYNAMICSWORLD_H
