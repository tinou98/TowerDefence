#include "dynamicsworld.h"

#include "entity.h"

using namespace Physics;

DynamicsWorld::DynamicsWorld() {
	/* The broadphase is an excellent place for eliminating object pairs that should not collide.
	 * This can be for performance or gameplay reasons.
	 */
	this->broadphase = new btDbvtBroadphase();

	/* You can use the collision dispatcher to register a callback that filters overlapping broadphase
	 * proxies so that the collisions are not processed by the rest of the system.
	 */
	this->collisionConfiguration = new btDefaultCollisionConfiguration();
	this->dispatcher = new btCollisionDispatcher(collisionConfiguration);

	/*  This is what causes the objects to interact properly, taking into account gravity,
	 * game logic supplied forces, collisions, and hinge constraints. It does a good job as long
	 * as you don't push it to extremes, and is one of the bottlenecks in any high performance simulation.
	 * There are parallel versions available for some threading models
	 */
	this->solver = new btSequentialImpulseConstraintSolver;

	/* Now, we can finally instantiate the dynamics world:
	 */
	this->dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	this->dynamicsWorld->setGravity(btVector3(0, 0, -10));

	this->dynamicsWorld->setDebugDrawer(&this->debug);

	f.begin();
}

DynamicsWorld::~DynamicsWorld() {
	for(auto e : this->entityList)
		this->dynamicsWorld->removeRigidBody(e);

	delete this->dynamicsWorld;
	delete this->solver;
	delete this->dispatcher;
	delete this->collisionConfiguration;
	delete this->broadphase;
}

void DynamicsWorld::addRigidBody(const Entity &rb) {
	this->entityList.push_back(rb.body);
	this->dynamicsWorld->addRigidBody(rb.body);
}
void DynamicsWorld::removeRigidBody(const Entity &rb) {
	this->entityList.remove(rb.body);
	this->dynamicsWorld->removeRigidBody(rb.body);
}

void DynamicsWorld::operator ()()  {
	this->dynamicsWorld->stepSimulation(10);
}

void DynamicsWorld::operator ()(glm::mat4 mat)  {
	this->debug(mat);

	this->dynamicsWorld->stepSimulation(f.end());
	f.begin();
	this->dynamicsWorld->debugDrawWorld();
}
