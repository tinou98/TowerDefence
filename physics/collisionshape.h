#ifndef COLLISIONSHAPE_H
#define COLLISIONSHAPE_H

#include <btBulletDynamicsCommon.h>

#include "motionstate.h"

#include <cstdint>

namespace Physics {

enum class CollisionType : std::uint8_t {
	UnknowType = 0,

	BvhTriangleMeshShape	= 1,
	BoxShape				= 2,
	SphereShape				= 3,
	CapsuleShape			= 4,
	CylinderShape			= 5,
	ConeShape				= 6,
	ConvexHullShape			= 7,
	CompoundShape			= 8,
};

struct ColisionShapeParam {
	ColisionShapeParam(float mass = 0.) : mass(mass) {}

	float mass;
	union {
		struct {} Empty;

		struct {

		} BvhTriangleMeshShape;

		struct {
			btVector3 boxHalfExtents;
		} BoxShape;

		struct {
			btScalar radius;
		} SphereShape;

		struct {
			btScalar radius;
			btScalar height;
		} CapsuleShape;

		struct {
			btVector3 halfExtents;
		} CylinderShape;

		struct {
			btScalar radius;
			btScalar height;
		} ConeShape;

		struct {

		} ConvexHullShape;

		struct {

		} CompoundShape;
	};
};

	class CollisionShape {
	public:
		explicit CollisionShape(btCollisionShape *newShape = nullptr);
		CollisionShape(CollisionType id, ColisionShapeParam param);
		~CollisionShape();

		btVector3 calculateLocalInertia() const;
		btRigidBody::btRigidBodyConstructionInfo getRigidBodyCreationInfo(MotionState *state) const;

		btScalar mass = 0.;

	private:
		CollisionShape(const CollisionShape &other) = delete;

		btCollisionShape *shape = nullptr;
	};

}

#include <iostream>
std::ostream &operator <<(std::ostream &os, const Physics::CollisionType &id);


#endif // COLLISIONSHAPE_H
