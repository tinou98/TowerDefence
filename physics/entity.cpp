#include "entity.h"

#include "collisionshape.h"
#include "dynamicsworld.h"

using namespace Physics;

Entity::Entity(btTransform pos, const CollisionShape &shape, btTransform offset) : state(pos, offset) {
	this->body = new btRigidBody(shape.getRigidBodyCreationInfo(&this->state));
}

Entity::~Entity() {
	delete this->body;
}

/*Entity::operator btRigidBody *() {
	return this->body;
}*/
