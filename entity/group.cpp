#include "group.h"

Group::Group() {}

void Group::operator ()(glm::mat4 mat) {
	for(const auto& e : this->lst3D)
		(*e)(mat);
}

void Group::operator ()(int width, int height) {
	for(const auto& e : this->lst2D)
		(*e)(width, height);
}

//void Group::addItem(Entity3D *item) { this->lst3D.push_back(item); }
void Group::addItem(Entity3D *item) { this->lst3D.push_front(item); }
//void Group::addItem(Entity2D *item) { this->lst2D.push_back(item); }
void Group::addItem(Entity2D *item) { this->lst2D.push_front(item); }
