#ifndef GROUP_H
#define GROUP_H

#include <entity/2D/entity2d.h>
#include <entity/3D/entity3d.h>

#include <vector>

class Group : public Entity3D, public Entity2D {
public:
	Group();

	void addItem(Entity3D *item);
	void addItem(Entity2D *item);

	void operator ()(glm::mat4 mat);
	void operator ()(int width, int height);

private:
//	std::vector<Entity3D*> lst3D;
	std::forward_list<Entity3D*> lst3D;
//	std::vector<Entity2D*> lst2D;
	std::forward_list<Entity2D*> lst2D;
};

#endif // GROUP_H
