#include "vertexarraybufferobject.h"

VertexArrayBufferObject::VertexArrayBufferObject(const std::string &shaderVertex, const std::string &shaderFragment) : vao(), vbo(), shader(shaderVertex, shaderFragment) {}

void VertexArrayBufferObject::setupVABO(const std::vector<glm::vec3> &vertex, const std::vector<glm::vec2> &uv) {
	this->shader();
	this->vao();
	this->vbo.addAndSetItems({
								 makeVBOrow(vertex, vboConfig(0, 3, GL_FLOAT, GL_FALSE, 0)),
								 makeVBOrow(uv, vboConfig(1, 2, GL_FLOAT, GL_FALSE, 0)),
							 });
	this->vao.clear();
}

void VertexArrayBufferObject::operator ()() {
	this->shader();
	this->vao();
	this->vbo();
}

