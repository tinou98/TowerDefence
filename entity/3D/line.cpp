#include "line.h"

#include <glm/gtc/type_ptr.hpp>

Line::Line(unsigned int size) : vABo("shaders/line.vert", "shaders/line.frag"), iMax(size) {
	this->vABo.shader();
	this->vABo.vao();

	this->vABo.vbo.initStatic(200 * 3 * sizeof(float), GL_DYNAMIC_STORAGE_BIT, nullptr);
	vboConfig(0, 3, GL_FLOAT, GL_FALSE, 0)();

	this->colorVBO.initStatic(200 * 3 * sizeof(float), GL_DYNAMIC_STORAGE_BIT, nullptr);
	vboConfig(1, 3, GL_FLOAT, GL_FALSE, 0)();
}

unsigned int Line::pushBack(btVector3 from, btVector3 to, btVector3 fromCol, btVector3 toCol) {
	this->vABo.vbo();
	glBufferSubData(this->vABo.vbo.getTarget(), i * 3 * sizeof(float), 3 * sizeof(float), (btScalar *)from);
	glBufferSubData(this->vABo.vbo.getTarget(), (i+1) * 3 * sizeof(float), 3 * sizeof(float), (btScalar *)to);


	this->colorVBO();
	glBufferSubData(this->colorVBO.getTarget(), i * 3 * sizeof(float), 3 * sizeof(float), (btScalar *)fromCol);
	glBufferSubData(this->colorVBO.getTarget(), (i+1) * 3 * sizeof(float), 3 * sizeof(float), (btScalar *)toCol);

	i += 2;
	return i;
}

void Line::operator()(glm::mat4 mat) {
	this->vABo();
//	Pipeline::uniformLoc colorLoc = this->vABo.shader.getUniformLocation("color");
//	glProgramUniform3f(colorLoc.program, colorLoc.uniformId, this->color.r, this->color.g, this->color.b);

	Pipeline::uniformLoc mvpLoc = this->vABo.shader.getUniformLocation("VP");
	glProgramUniformMatrix4fv(mvpLoc.program, mvpLoc.uniformId, 1, GL_FALSE, glm::value_ptr(mat));

	glBindTexture(GL_TEXTURE_2D, 0);

	glDrawArrays(GL_LINES, 0, i);

	i = 0;
}

