#include "loader.h"

#include <fstream>
#include <stdexcept>
#include <iostream>
#include <regex>

#include <glm/gtc/type_ptr.hpp>

#include <utils/fps.h>
#include <utils/txtutils.h>

SETUP_LOG(Loader, "LOADER")

float toFloat(std::string s, float defVal = 0) {
	try {
		return std::atof(s.c_str());
	} catch(...) {
		return defVal;
	}
}
float toInt(std::string s, float defVal = 0) {
	try {
		return std::atoi(s.c_str());
	} catch(...) {
		return defVal;
	}
}

Loader::Loader(std::string filePath) {
	Fps f;

	std::vector <glm::vec3> verticesTmp;
	std::vector <glm::vec2> uvTmp;
	std::vector <glm::vec3> normalsTmp;

	uvTmp.push_back(glm::vec2(0, 0));
	normalsTmp.push_back(glm::vec3(0, 0, 0));

	std::vector <glm::vec3> vertices;
	std::vector <glm::vec2> uv;
	std::vector <glm::vec3> normals;

	std::ifstream file(filePath);

	setup(LOG, file.is_open(), "Opening file " + filePath);

	std::string line;

	std::regex vertex(		R"(v(?:\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?))(?:\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?))(?:\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?)))"),
			vertexTexture(	R"(vt\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?)\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?))"),
			vertexNormal(	R"(vn\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?)\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?)\s+(-?(?:(?:\d+(?:\.\d*)?)|(?:\d*(?:\.\d+)))(?:e-?\d+)?))"),
			face(			R"(f)"),
			faceInternal(	R"((\d+)(?:/(\d+)?(?:/(\d*))?)?)"),
			mtlLib(			R"(mtllib\s+(.*))"),
			useMtl(			R"(usemtl\s+(.*))"),
			object(			R"(o\s+(.*))"),
			group(			R"(g\s+(.*))");

	std::smatch match;

	std::string currObject = "",
				currGroup = "";
	int lastPosSaved = 0;

	while(getline(file, line)) {
		trim(line);
		if(line.empty()) continue;
		if(line[0] == '#') continue;

		if(std::regex_search(line, match, vertex)) {
			glm::vec3 vertex;

			vertex.x = toFloat(match[1]);
			vertex.y = toFloat(match[2]);
			vertex.z = toFloat(match[3]);

			verticesTmp.push_back(vertex);
		} else if(std::regex_search(line, match, vertexTexture)) {
			glm::vec2 vertex;

			vertex.x = toFloat(match[1]);
			vertex.y = toFloat(match[2]);

			uvTmp.push_back(vertex);
		} else if(std::regex_search(line, match, vertexNormal)) {
			glm::vec3 normal;

			normal.x = toFloat(match[1]);
			normal.y = toFloat(match[2]);
			normal.z = toFloat(match[3]);

			normalsTmp.push_back(normal);
		} else if(std::regex_search(line, match, face)) {
			while(std::regex_search(line, match, faceInternal)) {
				int vertex =	(toInt(match[1]) - 1),
					texture =	(toInt(match[2])), // No -1 because default included
					normal =	(toInt(match[3])); // No -1 because default included

				vertices.push_back(verticesTmp.at(vertex));
				uv.push_back(uvTmp.at(texture));
				normals.push_back(normalsTmp.at(normal));

				line = match.suffix().str();
			}
		} else if(std::regex_search(line, match, mtlLib))
			this->mtl.load(filePath.substr(0, filePath.find_last_of("/")+1) + trimCopy(match[1]));

		else if(std::regex_search(line, match, useMtl)) {
			if(lastPosSaved < vertices.size()) {
				this->objectsInternalLimit[currObject][currGroup].push_front({range(lastPosSaved, vertices.size()), {this->mtl.itemLocked(), locRotScale()}});
				lastPosSaved = vertices.size();
			}

			this->mtl.lockItem(trimCopy(match[1]));
		} else if(std::regex_search(line, match, object)) {
			if(lastPosSaved < vertices.size()) {
				this->objectsInternalLimit[currObject][currGroup].push_front({range(lastPosSaved, vertices.size()), {this->mtl.itemLocked(), locRotScale()}});
				lastPosSaved = vertices.size();
			}

			currObject = match[1];
			currGroup = "";
		} else if(std::regex_search(line, match, group)) {
			if(lastPosSaved < vertices.size()) {
				this->objectsInternalLimit[currObject][currGroup].push_front({range(lastPosSaved, vertices.size()), {this->mtl.itemLocked(), locRotScale()}});
				lastPosSaved = vertices.size();
			}

			currGroup = match[1];
		} else {
			LOG.fail(SSTR("Unknow row " << line));
		}
	}

	this->objectsInternalLimit[currObject][currGroup].push_front({range(lastPosSaved, vertices.size()), {this->mtl.itemLocked(), locRotScale()}});

	this->vabo.setupVABO(vertices, uv);

	LOG.done(SSTR("Loaded " << vertices.size() << " vertices in " << 1000*f.end() << "ms"));
}

void Loader::operator ()(glm::mat4 mat) {
	mat *= this->state.toMat4();
	this->vabo();
	Entity3D::operator ()(mat);


	Pipeline::uniformLoc colorLoc = this->vabo.shader.getUniformLocation("color");
	Pipeline::uniformLoc mvpLoc = this->vabo.shader.getUniformLocation("VP");

	for(const auto& object : this->objectsInternalLimit) {
		glm::mat4 objMat = mat;
		for(const auto& group : object.second) {
			glProgramUniformMatrix4fv(mvpLoc.program, mvpLoc.uniformId, 1, GL_FALSE, glm::value_ptr(objMat));

			for(std::pair<range, std::pair<std::string, locRotScale>> material : group.second) {
				this->mtl.lockItem(material.second.first);
				if(!this->mtl.getMaterial().mapKd.empty())
					(*this->mtl.getImage())();
				else
					glBindTexture(GL_TEXTURE_2D, 0);

				glm::vec3 color = this->mtl.getColor();
				glProgramUniform3f(colorLoc.program, colorLoc.uniformId, color.r, color.g, color.b);

				glDrawArrays(GL_TRIANGLES, material.first.from, material.first.to - material.first.from);
			}
		}
	}
}

