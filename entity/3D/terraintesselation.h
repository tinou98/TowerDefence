#ifndef TERRAINTESSELATION_H
#define TERRAINTESSELATION_H

#include "entity3d.h"
#include <loader/image.h>
#include <entity/vertexarraybufferobject.h>

class TerrainTesselation : public Entity3D
{
public:
	TerrainTesselation();
	void operator()(glm::mat4 mat);

private:
	VertexArrayBufferObject vabo;
	GLuint UBO;
	Image heightMap;
};

#endif // TERRAINTESSELATION_H
