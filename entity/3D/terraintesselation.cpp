#include "terraintesselation.h"

struct {
	glm::vec2 size = glm::vec2(120, 120);
	glm::ivec2 divisionNum = glm::ivec2(64, 64);
	glm::mat4 mat;
	float amplitude = 15;
} param;

TerrainTesselation::TerrainTesselation() : vabo("shaders/terrain.vert", "shaders/terrain.frag"), heightMap("model/hm.dds") {
	this->vabo.shader.addShader(new Shader(GL_TESS_CONTROL_SHADER, "shaders/terrain.tcs"));
	this->vabo.shader.addShader(new Shader(GL_TESS_EVALUATION_SHADER, "shaders/terrain.tes"));

	this->vabo.shader.addShader(new Shader(GL_GEOMETRY_SHADER, "shaders/wireframe.geo"));

	this->vabo.setupVABO({glm::vec3(0, 0, 0)}, {glm::vec2(0)});



	glGenBuffers(1, &this->UBO);
	glBindBuffer(GL_UNIFORM_BUFFER, this->UBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(param), &param, GL_STREAM_DRAW);

	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void TerrainTesselation::operator()(glm::mat4 mat) {
	param.mat = mat;
	mat *= this->state.toMat4();

	this->vabo();
	Entity3D::operator ()(mat);

	glPatchParameteri( GL_PATCH_VERTICES, 1);

	glBindBuffer(GL_UNIFORM_BUFFER, this->UBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(param), &param, GL_STREAM_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, this->UBO);

	glBindTexture(GL_TEXTURE_2D, this->heightMap);

	int instances = param.divisionNum.x*param.divisionNum.y;
	glDrawArraysInstanced(GL_PATCHES, 0, 1, instances);

	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}
