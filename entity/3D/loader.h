#ifndef LOADER_H
#define LOADER_H

#include <entity/3D/entity3d.h>
#include <entity/vertexarraybufferobject.h>

#include <glm/glm.hpp>
#include <map>
#include <forward_list>

#include <utils/log.h>
#include <loader/mtlloader.h>

class range {
public:
	range(int from, int to) : from(from), to(to) {}
	int from;
	int to;
};

class Loader : public Entity3D {
public:
	explicit Loader(std::string filePath);

	void operator ()(glm::mat4 mat);

private:
	ADD_DEFAULT_LOG

	MtlLoader mtl;

	std::map<std::string, std::map<std::string, std::forward_list<std::pair<range, std::pair<std::string, locRotScale>>>>> objectsInternalLimit;
	//		ObjectName				Group Name								Range				MtlName		transformation

	VertexArrayBufferObject vabo;
};

#endif // LOADER_H
