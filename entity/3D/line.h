#ifndef LINE_H
#define LINE_H

#include <entity/3D/entity3d.h>
#include <entity/vertexarraybufferobject.h>

#include <LinearMath/btVector3.h>

class Line : public Entity3D
{
public:
	explicit Line(unsigned int size);
	unsigned int pushBack(btVector3 from, btVector3 to, btVector3 fromCol, btVector3 toCol);

	virtual void operator()(glm::mat4 mat);

private:
	unsigned int i = 0;
	unsigned int iMax = 0;

	VertexArrayBufferObject vABo;
	VBO colorVBO;
};

#endif // LINE_H
