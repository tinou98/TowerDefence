#ifndef ENTITY3D_H
#define ENTITY3D_H

#include <glm/glm.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <entity/2D/entity2d.h>
#include <forward_list>

#include <iostream>

#include <physics/motionstate.h>
#include <physics/entity.h>
#include <physics/collisionshape.h>
#include <LinearMath/btTransform.h>

class locRotScale {
public:
	locRotScale(glm::vec3 loc = glm::vec3(0, 0, 0), glm::vec3 rot = glm::vec3(0, 0, 0), glm::vec3 scale = glm::vec3(1, 1, 1)) : loc(loc), rot(rot), scale(scale) {}

	glm::vec3 loc;
	glm::vec3 rot;
	glm::vec3 scale;
};

class Entity3D : public Physics::Entity {
public:
	Entity3D(const btTransform& position = btTransform::getIdentity(), const Physics::CollisionShape &shape = Physics::CollisionShape(Physics::CollisionType::UnknowType, {}), const btTransform& centerOfMassOffset = btTransform::getIdentity()) : Physics::Entity(position, shape, centerOfMassOffset) {}

	virtual void operator()(glm::mat4 mat) {
		mat *= this->state.toMat4();
		for(const auto& p : this->elem2D) {
			glm::vec4 finPos = glm::vec4(mat * glm::vec4(p.second, 1));

			p.first->setAbsPos(finPos.xy()/finPos.ww());
//			std::cout << "POS : " << finPos.x << ";" << finPos.y << ";" << finPos.z << ";" << finPos.w << std::endl;

			p.first->visible = (finPos.z > 0);
		}
	}

	/* Transform */
	void add2DItem(Entity2D *entity, glm::vec3 pos) {this->elem2D.push_front({entity, pos});}
private:
	std::forward_list<std::pair<Entity2D*, glm::vec3>> elem2D;
};

#endif // ENTITY3D_H
