#ifndef ARCCIRCLE_H
#define ARCCIRCLE_H

#include <entity/2D/entity2d.h>
#include <entity/vertexarraybufferobject.h>

class ArcCircle : public Entity2D
{
public:
	ArcCircle(float radius = 0, int nbPoints = 15);

	virtual void operator()(int width, int height);

	float getRadius() const;
	void setRadius(float radius);

	unsigned short getNbPoints() const;
	void setNbPoints(unsigned short nbPoints);


private:
	float radius;
	unsigned short nbPoints;

	void updateInternal();
	VertexArrayBufferObject vABo;
};

#endif // ARCCIRCLE_H
