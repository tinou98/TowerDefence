#include "text2d.h"

#include <glm/gtc/type_ptr.hpp>

SETUP_LOG(Text2D, "TEXT-LOADER")

Text2D::Text2D(Font *font) : font(font), txt("NULL"), prerendered(false), render(0, 0, false, GL_RED), vaboPre("shaders/text.vert", "shaders/text.frag"), vaboDisp("shaders/vertex_.vert", "shaders/fragment_.frag") {
	this->vaboPre();

	std::vector<glm::vec4> empty(4*6);
	vboConfig c(0, 4, GL_FLOAT, GL_FALSE, 0);
	this->vaboPre.vbo.addAndSetItems({makeVBOrow(empty, c)});
}

void Text2D::setText(std::string s) {
	prerendered = false;
	this->txt = s;

	prerender();
}

bool Text2D::prerender() {
	LOG.info("Start prerender");

	if(this->font == 0) {
		LOG.fail("Can't prerender : Font object is empty");
		return false;
	}

	Char ch;
	// Get size
	int heightDOWN = 0, heightUP = 0, width = 0;
	for(char c : this->getText()) {
		ch = this->font->getGlyph(c);

		if(heightUP < ch.bearing.y) heightUP = ch.bearing.y;
		if(heightDOWN < ch.size.y - ch.bearing.y) heightDOWN = ch.size.y - ch.bearing.y;
		width += (ch.advance >> 6);
	}

	/* Setup display VA/BO */ {
		float width2 = width/2;

		std::vector<glm::vec3> vertex;
		std::vector<glm::vec2> uv;

		vertex.push_back(glm::vec3(-width2, -heightDOWN, 0));	uv.push_back(glm::vec2(0, 0));
		vertex.push_back(glm::vec3( width2,  heightUP, 0));		uv.push_back(glm::vec2(1, 1));
		vertex.push_back(glm::vec3(-width2,  heightUP, 0));		uv.push_back(glm::vec2(0, 1));

		vertex.push_back(glm::vec3( width2,  heightUP, 0));		uv.push_back(glm::vec2(1, 1));
		vertex.push_back(glm::vec3(-width2, -heightDOWN, 0));	uv.push_back(glm::vec2(0, 0));
		vertex.push_back(glm::vec3( width2, -heightDOWN, 0));	uv.push_back(glm::vec2(1, 0));
		this->vaboDisp.setupVABO(vertex, uv);
	}


	auto height = heightUP + heightDOWN;
//	this->render.resize(width, height);

	render.resize(width, height);
	Locker l(render);

	this->vaboPre();

	Pipeline::uniformLoc resize = this->vaboPre.shader.getUniformLocation("resize");
	glProgramUniformMatrix4fv(resize.program, resize.uniformId, 1, GL_FALSE, glm::value_ptr(glm::scale(glm::translate(glm::mat4(1), glm::vec3(-1, -1, 0)), glm::vec3(2.0/(float)width, 2.0/(float)height, 1.0))));

	float x = 0;
	for(char c : this->getText()) {
		ch = this->font->getGlyph(c);

		GLfloat w = ch.size.x;
		GLfloat h = ch.size.y;

		// Down Left point
		GLfloat xpos = x + ch.bearing.x;
		GLfloat ypos = ch.bearing.y - ch.size.y			+ heightDOWN;

		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos		, ypos		,	0, 1 },
			{ xpos + w	, ypos + h	,	1, 0 },
			{ xpos		, ypos + h	,	0, 0 },

			{ xpos + w	, ypos + h	,	1, 0 },
			{ xpos		, ypos		,	0, 1 },
			{ xpos + w	, ypos		,	1, 1 },
		};

		glBindTexture(GL_TEXTURE_2D, ch.textureID);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glDrawArrays(GL_TRIANGLES, 0, 6);


		x += ch.advance >> 6;
	}

	return (prerendered = true);
}

#include <utils/glmutils.h>
#include <glm/gtc/matrix_transform.hpp>
void Text2D::operator ()(int width, int height) {
	if(!this->visible) return;

	if(!prerendered && !prerender()) return;

	this->vaboDisp();


	this->render.bindTexture();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


	glm::mat4 m = this->getInternalMatrice(width, height);
	Pipeline::uniformLoc mvpLoc = this->vaboDisp.shader.getUniformLocation("MVP");
	glProgramUniformMatrix4fv(mvpLoc.program, mvpLoc.uniformId, 1, GL_FALSE, glm::value_ptr(m));
	Pipeline::uniformLoc colorLoc = this->vaboDisp.shader.getUniformLocation("color");
	glProgramUniform3f(colorLoc.program, colorLoc.uniformId, this->color.r, color.g, color.b);

	glDrawArrays(GL_TRIANGLES, 0, 6);
}
