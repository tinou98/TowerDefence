#ifndef TEXT2D_H
#define TEXT2D_H

#include <entity/2D/entity2d.h>
#include <utils/log.h>
#include <shader/shader.h>

#include <font/font.h>

#include <utils/rendertotexture.h>
#include <entity/vertexarraybufferobject.h>

#include <loader/image.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Text2D : public Entity2D {
public:
	explicit Text2D(Font *font);

	void operator ()(int width, int height);


	void setText(std::string s);
	std::string getText() {return this->txt;}

	Font* getFont() {return this->font;}
	void setFont(Font *font) {this->font = font; prerendered = false; prerender(); }

	bool prerender();
private:
	ADD_DEFAULT_LOG
	Font *font;
	std::string txt;


	bool prerendered;
	renderToTexture render;

	VertexArrayBufferObject vaboPre, vaboDisp;
};

#endif // TEXT2D_H
