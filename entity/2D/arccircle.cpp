#include "arccircle.h"

#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

ArcCircle::ArcCircle(float radius, int nbPoints) : radius(radius), nbPoints(nbPoints) {
	this->updateInternal();
}

void ArcCircle::operator()(int width, int height) {
	this->vABo();
	Pipeline::uniformLoc mvpLoc = this->vABo.shader.getUniformLocation("MVP");
	glProgramUniformMatrix4fv(mvpLoc.program, mvpLoc.uniformId, 1, GL_FALSE, glm::value_ptr(this->getInternalMatrice(width, height)));
	Pipeline::uniformLoc colorLoc = this->vABo.shader.getUniformLocation("color");
	glProgramUniform3f(colorLoc.program, colorLoc.uniformId, this->color.r, this->color.g, this->color.b);
	glBindTexture(GL_TEXTURE_2D, 0);


	glDrawArrays(GL_LINE_LOOP, 0, this->getNbPoints());
}

float ArcCircle::getRadius() const				{ return this->radius; }
unsigned short ArcCircle::getNbPoints() const	{ return this->nbPoints; }

void ArcCircle::setRadius(float radius)				 { this->radius = radius;		 this->updateInternal();}
void ArcCircle::setNbPoints(unsigned short nbPoints) { this->nbPoints = nbPoints; this->updateInternal();}

void ArcCircle::updateInternal() {
	std::vector<glm::vec3> vertex(this->getNbPoints());

	float angle = 2 * M_PI / this->getNbPoints();
	for(int i = 0; i < this->getNbPoints(); ++i) {
		vertex.at(i) = glm::vec3(std::cos(i*angle) * this->getRadius(),
								 std::sin(i*angle) * this->getRadius(),
								 0);
	}

	this->vABo.vao();
	this->vABo.vbo.addAndSetItems({makeVBOrow(vertex, vboConfig(0, 3, GL_FLOAT, GL_FALSE, 0))});
	VAO::clear();
}
