#ifndef ENTITY2D_H
#define ENTITY2D_H

#include <glm/glm.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>

#define getSet(type, var, def, get, set) protected: type var = def; public: type get() const {return this->var;} void set(const type &newVal) {this->needRecalculate = true; this->var = newVal;};

class Entity2D {
public:
	Entity2D() {}

	virtual void operator()(int width, int height) = 0;

	void show() {this->visible = true; }
	void hide() {this->visible = false; }
	bool visible = true;

	getSet(glm::vec2, absPos, glm::vec2(0), getAbsPos, setAbsPos)
	getSet(glm::vec2, relPos, glm::vec2(0), getRelPos, setRelPos)
	getSet(glm::vec2, scale, glm::vec2(1), getScale, setScale)

	getSet(glm::vec3, color, glm::vec3(1, 1, 1), getColor, setColor)

	getSet(float, rot, 0, getRot, setRot)

protected:
		mutable bool needRecalculate;
		mutable glm::mat4 internalMatrice;

		void updateInternalMatrice() const {
			float cos = cosf(this->getRot()* M_PI / 180),
					sin = sinf(this->getRot() * M_PI / 180);

			internalMatrice = glm::mat4(
							this->getScale().x*cos, this->getScale().x*sin, 0, 0,
							-this->getScale().y*sin, this->getScale().y*cos, 0, 0,
							0, 0, 0, 0,
							this->getAbsPos().x, this->getAbsPos().y, 0, 1
							);

			this->needRecalculate = false;
		}

		glm::mat4 getInternalMatrice(int width, int height) const {
			if(this->needRecalculate) this->updateInternalMatrice();

			glm::mat4 ret;

			ret = this->internalMatrice;

			ret[0][0] /= (float)width;
			ret[0][1] /= (float)width;
			ret[1][0] /= (float)height;
			ret[1][1] /= (float)height;

			ret[3][0] += this->getRelPos().x/width;
			ret[3][1] += this->getRelPos().y/height;

//			ret[3][0] *= this->getScale().x;
//			ret[3][1] *= this->getScale().y;

			return ret;
		}
};

#endif // ENTITY2D_H
