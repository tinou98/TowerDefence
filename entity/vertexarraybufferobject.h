#ifndef VERTEXARRAYBUFFEROBJECT_H
#define VERTEXARRAYBUFFEROBJECT_H

#include <vector>

#include <glm/glm.hpp>

#include <utils/vao.h>
#include <utils/buffer/vbo.h>
#include <shader/pipeline.h>

class VertexArrayBufferObject {
public:
	VertexArrayBufferObject(const std::string &shaderVertex = "shaders/vertex.vert", const std::string &shaderFragment = "shaders/fragment.frag");

	void operator ()();
	void setupVABO(const std::vector<glm::vec3> &vertex, const std::vector<glm::vec2> &uv);

	VAO vao;
	VBO vbo;

	Pipeline shader;
};

#endif // VERTEXARRAYBUFFEROBJECT_H
