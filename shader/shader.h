#ifndef DEF_SHADER
#define DEF_SHADER

#include <utils/log.h>

#include <GL/glew.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>

#include <string>
#include <fstream>


class Shader {
public:
	Shader(GLenum shaderType, std::string fileOrString);
	~Shader();

	void load(GLenum shaderType, std::string fileOrString);
	GLuint compileShader(GLenum type, std::string &source);

	void operator ()() const { glUseProgram(*this); }
	operator GLuint() const { return this->programID; }

	GLenum getProgID() const { return this->programID; }
	GLenum getType() const { return this->shaderType; }
	GLbitfield getBitfield() const;

private:
	Log log;

	GLenum shaderType;
	GLuint programID = 0;
};

#endif
