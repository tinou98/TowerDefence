#include "shader.h"


std::string getShaderType(GLenum type) {
	switch(type) {
		case GL_VERTEX_SHADER:
			return "VERTEX";
		case GL_TESS_CONTROL_SHADER:
			return "TESSELATION-CONTROL";
		case GL_TESS_EVALUATION_SHADER:
			return "TESSELATION-EVAL";
		case GL_GEOMETRY_SHADER:
			return "GEOMETRY";
		case GL_FRAGMENT_SHADER:
			return "FRAGMENT";
		case GL_COMPUTE_SHADER:
			return "COMPUTE";

		default:
			return "NON-SHADER";
	}
}

Shader::Shader(GLenum shaderType, std::string fragmentFile) : shaderType(shaderType), log("SHADER-" + getShaderType(shaderType)) {
	std::ifstream in(fragmentFile, std::ios::in);
	log.info(SSTR("Start compile : " << fragmentFile));
	if(in.is_open()) {
		log.info("Compile from file");
		std::stringstream buff;
		buff << in.rdbuf();
		fragmentFile = buff.str();
	} else
		log.info("Compile from inline string");

//	const GLchar* fullSrc[2] = { shaderPrefix, src };
	const GLchar* fullSrc[] = { &fragmentFile.at(0) };
	this->programID = glCreateShaderProgramv(shaderType, 1, (const GLchar **)fullSrc);

	// Check link
	GLint linkNoError(0);
	glGetProgramiv(this->programID, GL_LINK_STATUS, &linkNoError);

	setupNoRet(log, linkNoError == GL_TRUE, "Link",);

	GLint errorSize(0);
	glGetProgramiv(this->programID, GL_INFO_LOG_LENGTH, &errorSize);
	if(errorSize > 1) {
		char *error = new char[errorSize+1];
		glGetProgramInfoLog(this->programID, errorSize, &errorSize, error);

		// Affichage de l'erreur
		log.fail(error);

		delete[] error;

		if(linkNoError != GL_TRUE)
			glDeleteProgram(this->programID);
	}
}

Shader::~Shader() {
	glDeleteProgram(this->programID);
}

GLbitfield Shader::getBitfield() const {
	switch(this->getType()) {
		case GL_VERTEX_SHADER:			return GL_VERTEX_SHADER_BIT;
		case GL_TESS_CONTROL_SHADER:	return GL_TESS_CONTROL_SHADER_BIT;
		case GL_TESS_EVALUATION_SHADER:	return GL_TESS_EVALUATION_SHADER_BIT;
		case GL_GEOMETRY_SHADER:		return GL_GEOMETRY_SHADER_BIT;
		case GL_FRAGMENT_SHADER:		return GL_FRAGMENT_SHADER_BIT;
		case GL_COMPUTE_SHADER:			return GL_COMPUTE_SHADER_BIT;
		default:						return 0;
	}
}
