#include "pipeline.h"

SETUP_LOG(Pipeline, "PIPELINE")

Pipeline::Pipeline() {
	glGenProgramPipelines(1, &this->progPipeline);
}

Pipeline::Pipeline(std::string vertex, std::string fragment) : Pipeline() {
	this->addShader(new Shader(GL_VERTEX_SHADER, vertex));
	this->addShader(new Shader(GL_FRAGMENT_SHADER, fragment));
}

Pipeline::~Pipeline() {
	for(const auto& i : this->shaderList)
		delete i;
}

void Pipeline::addShader(const Shader *shader) {
	glUseProgramStages(*this, shader->getBitfield(), shader->getProgID());

	this->shaderList.push_back(shader);
}

Pipeline::uniformLoc Pipeline::getUniformLocation(const GLchar *name, GLuint program) {
	if(program != -1)
		return {program, glGetUniformLocation(program, name)};

	for(const Shader* i : this->shaderList)
		if(glGetUniformLocation(*i, name) != -1)
			return {i->operator GLuint(), glGetUniformLocation(*i, name)};


	LOG.fail(SSTR("Searched " << name << " was not found"));

	return {0, -1};
}

Pipeline::operator GLuint() {return this->progPipeline;}
void Pipeline::operator ()() {glBindProgramPipeline(*this);}
