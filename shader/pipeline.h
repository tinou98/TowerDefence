#ifndef PIPELINE_H
#define PIPELINE_H

#include <GL/glew.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>

#include "shader.h"
#include <utils/log.h>

#include <vector>

class Pipeline
{
public:
	Pipeline();
	Pipeline(std::string vertex, std::string fragment);
	~Pipeline();
	void addShader(const Shader *shader);

	struct uniformLoc {
		GLuint program;
		GLint uniformId;
	};
	uniformLoc getUniformLocation(const GLchar *name, GLuint program = -1);

	void operator()();
	operator GLuint();

private:
	ADD_DEFAULT_LOG
	GLuint progPipeline = 0;

	std::vector<const Shader*> shaderList;
};

#endif // PIPELINE_H
