#! /bin/bash
set -x

apt-get update -qq && apt install -y -qq xorg-dev libglew-dev libfreetype6-dev libopenal-dev libsndfile1-dev unzip cmake


# Manual download dep
[ -d dep ] || mkdir dep

if [ -d dep/glm ] ; then
    wget https://github.com/g-truc/glm/releases/download/0.9.8.4/glm-0.9.8.4.zip -c -O glm.zip
    unzip glm.zip
    mv glm/glm dep/glm
fi

# Download GLFW
[ -d glfw ] || git clone https://github.com/glfw/glfw.git

cd glfw && echo "Current dir : $PWD"
git pull
[ -d build ] || mkdir build
[ -d build/$BUILD_TYPE ] || mkdir build/$BUILD_TYPE
cd build/$BUILD_TYPE && echo "Current dir : $PWD"

cmake ../.. -DCMAKE_BUILD_TYPE=$BUILD_TYPE
make -j4
make install
cd ../../.. && echo "Current dir : $PWD"

# Download Bullet3
[ -d bullet3 ] || git clone https://github.com/bulletphysics/bullet3

cd bullet3 && echo "Current dir : $PWD"
git pull
[ -d build ] || mkdir build
[ -d build/$BUILD_TYPE ] || mkdir build/$BUILD_TYPE
cd build/$BUILD_TYPE && echo "Current dir : $PWD"
cmake ../.. -DBUILD_BULLET2_DEMOS=OFF -DBUILD_CLSOCKET=OFF -DBUILD_CPU_DEMOS=OFF -DBUILD_ENET=OFF -DBUILD_OPENGL3_DEMO=OFF -DBUILD_UNIT_TESTS=OFF -DCMAKE_BUILD_TYPE=$BUILD_TYPE
make -j4
make install
cd ../../.. && echo "Current dir : $PWD"