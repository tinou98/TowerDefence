#include <utils/log.h>
#include <openglctx.h>
#include <camera/firstPersonCamera.h>
#include <camera/lockedcamera.h>
#include <entity/3D/loader.h>
#include <entity/3D/terraintesselation.h>
#include <entity/2D/text2d.h>
#include <entity/2D/arccircle.h>

#include <font/fontmanager.h>
#include <sound/soundmngr.h>
#include <sound/soundsource.h>
#include <sound/soundbuffermusic.h>
#include <sound/soundbuffersmall.h>

#include <chrono>
#include <iostream>
#include <iomanip>

#include <utils/txtutils.h>
#include <utils/rendertotexture.h>
#include <entity/vertexarraybufferobject.h>
#include <glm/gtc/type_ptr.hpp>

#include <loader/loaderownfile.h>



#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <loader/imagearray.h>
#include <utils/fps.h>

#include <physics/dynamicsworld.h>
#include <physics/entity.h>
#include <physics/collisionshape.h>

#include <physics/ragdoll.h>

int main(int, char *[]) {
	std::set_terminate([]() {
		std::cout << std::endl <<
					 "Salut !!" << std::endl <<
					 "" << std::endl <<
					 "Il semblerait qu'il y ait un 'petit' probleme :)" << std::endl <<
					 "L'application a crash quoi !" << std::endl <<
					 "Le probleme c'est que je sait meme pas pourquoi :(" << std::endl <<
					 "Si tu veux m'aider, regarde quelque ligne plus tot un message en rouge" << std::endl <<
					 "Lui il peut te dire pourquoi, si tu veut m'aider envoit moi toutes (au moins la ligne rouge) les lignes au dessus !" << std::endl <<
					 "" << std::endl <<
					 "Encore desole, et bon jeu (qui ne marche pas)" << std::endl;
		exit(EXIT_FAILURE);
	});
	openGlCtx glCtx(4, 5);
	Log log("MAIN");

#if 1 == 1
	FirstPersonCamera cam(glCtx, glm::vec3(4, 4, -3));/*
	LockedCamera cam(glCtx, glm::vec3(5, 2, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));//*/

	LoaderOwnFile l("script.b");
	//	Fps f;

	glm::mat4 view = glm::mat4(1), projection;


//	bool spawned = false;

//	Physics::DynamicsWorld dynamicsWorld;

//	EntityProxy* cube = l.createMesh("Cube");
	EntityProxy* cube = l.createMesh("steve");
	cube->poseHandler->setPose(cube->boneHandler, 0, 0);

//	cube->updatePosition(glm::translate(glm::mat4(1), glm::vec3(0, 5, 0)));
//	dynamicsWorld.addRigidBody(*cube);
	//	cube->setScale(0.2);

	//	EntityProxy* sphere = l.createMesh("Sphere");
	//	sphere->setScale(0.2);


//	Physics::ColisionShapeParam p = {};
//	std::vector<Physics::CollisionShape *> shape;

//	p.SphereShape = {0.2};
//	shape.push_back(new Physics::CollisionShape(Physics::CollisionType::SphereShape, p));

//	p.BoxShape = {btVector3(25, 2, 25)};
//	shape.push_back(new Physics::CollisionShape(Physics::CollisionType::BoxShape, p));

	/* Plane */
//	btQuaternion q(0, 0, 0, 1);
//	q.setEuler(0, 10, 0);
//	Physics::Entity ground(btTransform(q, btVector3(0, 0, -5)), *shape[1]);
//	dynamicsWorld.addRigidBody(ground);

	/* Sphere */
//	Physics::Entity fall(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 10)), *shape[0]);
//	dynamicsWorld.addRigidBody(fall);

//	fall.operator btRigidBody *()->applyCentralImpulse(btVector3( 0.f, 0.f, -15));

	//	Physics::RagDoll r(dynamicsWorld.dynamicsWorld, btVector3(0, 0, 0), 2);

	while(!glfwWindowShouldClose(glCtx) && glfwGetKey(glCtx, GLFW_KEY_ESCAPE) != GLFW_PRESS) {
		//		f.begin();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        projection = glm::perspective(45.0f, 1920.f/1080.f, 0.01f, 5*100.0f);

		/* Process camera */ {
			double x, y;
			glfwGetCursorPos(glCtx, &x, &y);

			view = cam.update(x, y);
		}

//		dynamicsWorld(projection * view);

		//		cube->setPos(cam.getPos() + 4.0f * glm::normalize(cam.getDir()));
		//		sphere->setPos(cam.getPos() + 4.0f * glm::normalize(cam.getDir()) + glm::vec3(1, 0, 0));

		/*		if(glfwGetKey(glCtx, GLFW_KEY_SPACE)) {
			if(!spawned || glfwGetKey(glCtx, GLFW_KEY_C)) {
				spawned = true;
				cube = l.createMesh("Cube");
				cube->setScale(0.2);
				cube->setPos(cam.getPos() + 4.0f * glm::normalize(cam.getDir()));

				sphere = l.createMesh("Sphere");
				sphere->setScale(0.2);
				sphere->setPos(cam.getPos() + 4.0f * glm::normalize(cam.getDir()) + glm::vec3(1, 0, 0));
			}
		} else {
			spawned = false;
		}*/

		//		l.SSBOPerObject.flushMap();
		l.render(projection * view);
		glFlush();
		//		glMemoryBarrier(GL_ALL_BARRIER_BITS);
		// Next frame can start rendering ensure that everithing has been read before start updating


		glfwSwapBuffers(glCtx);
		glfwPollEvents();
	}


//	dynamicsWorld.removeRigidBody(fall);
//	dynamicsWorld.removeRigidBody(ground);
//	dynamicsWorld.removeRigidBody(*cube);

	return 0;


#else

	//	Sound::Mngr soundMngr;
	//	Sound::BufferSmall laserBuff("laser.flac");
	//	Sound::BufferMusic smallBuff("out.flac");
	//	Sound::BufferMusic smallBuff2("_6out.flac");

	//	Sound::Source laser(&laserBuff);
	//	Sound::Source src(&smallBuff);
	//	Sound::Source src2(&smallBuff2);

	FirstPersonCamera cam(glCtx, glm::vec3(4, 4, -3));
	glCtx.cam = &cam;

	//	Loader model("model/Optimus/RB-OptimusBoss.obj");
	//	model.setScale(0.01);
	//	glCtx.group.addItem(&model);

	//#define MAX 0
#ifdef MAX
	Loader *l[MAX];
	for(int i = 0; i < MAX; i++) {
		l[i] = new Loader("model/Tour-laser1.2.obj");
		l[i]->setScale(0.5);
		l[i]->setPos(glm::vec3(i / 25, i % 25, 0));
		glCtx.group.addItem(l[i]);
	}
#endif
	/*
	FontManager *mngr = 0;
	Font *font = 0;
	try {
		mngr = new FontManager;

		font = mngr->getFont("font.ttf");
	} catch(const std::exception &e) {
		log.fail(SSTR("Failed while creating Text context : " << e.what()));
		font = 0;
	}

	Text2D t(font);
	t.setText("OptimusBoss le top du top");
	t.setColor(glm::vec3(0, 1, 0));
	t.setRot(15);
	t.setRelPos(glm::vec2(0, 100));
	model.add2DItem(&t, glm::vec3(-5.82769, 0, 815.55701));
//	model.add2DItem(&t, glm::vec3(53.63150, 0, 619.77301));
	glCtx.group.addItem(&t);
*/

	//	src.play();
	//	src2.play();

	//	TerrainTesselation te;
	//	glCtx.group.addItem(&te);


	//	ArcCircle c(5);
	//	t.add2DItem(&c, glm::vec3(0, 0, 0));

	glCtx();

#ifdef MAX
	for(int i = 0; i < MAX; i++)
		delete l[i];
#endif
	//delete mngr;
	return 0;
#endif
}
