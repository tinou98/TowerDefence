# FindLibSndFile

find_path(LIBSNDFILE_INCLUDE_DIR 
  NAMES sndfile.h sndfile.hh
  PATH_SUFFIXES include
)

find_library(LIBSNDFILE_LIBRARY
  NAMES sndfile sndfile-1
  PATH_SUFFIXES lib64 lib libs64 libs libs/Win32 libs/Win64 x86_64-linux-gnu lib/x86_64-linux-gnu
)


# handle the QUIETLY and REQUIRED arguments and set LIBSNDFILE_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LibSndFile  DEFAULT_MSG  LIBSNDFILE_LIBRARY LIBSNDFILE_INCLUDE_DIR)

mark_as_advanced(LIBSNDFILE_LIBRARY LIBSNDFILE_INCLUDE_DIR)
