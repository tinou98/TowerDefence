# FindLibSndFile

find_path(GLM_INCLUDE_DIR 
  NAMES glm/glm.hpp glm/ext.hpp
  PATH_SUFFIXES include
)


# handle the QUIETLY and REQUIRED arguments and set LIBSNDFILE_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(glm  DEFAULT_MSG  GLM_INCLUDE_DIR)

mark_as_advanced(LIBGLM_INCLUDE_DIR)
