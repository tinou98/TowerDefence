#include "openglctx.h"

#include <string>

#include <utils/fps.h>

#ifdef GL_VERSION_4_5
void APIENTRY openGLCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const GLchar* message, const void* userParam){
#else
#warning Using old function
void APIENTRY openGLCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const GLchar* message, void* userParam){
#endif
	const Log *log = static_cast<const Log*>(userParam);
	std::stringstream str;
	switch (source) {
		case GL_DEBUG_SOURCE_API:				str << "API"; break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		str << "WINDOWS_SYSTEM"; break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:	str << "SHADER_COMPILER"; break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:		str << "THIRD_PARTY"; break;
		case GL_DEBUG_SOURCE_APPLICATION:		str << "APPLICATION"; break;
		case GL_DEBUG_SOURCE_OTHER:				str << "OTHER"; break;
		default:								str << "UNKNOW"; break;
	}
	str << " ";
	switch (type) {
		case GL_DEBUG_TYPE_ERROR:				str << "ERROR"; break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:	str << "DEPRECATED_BEHAVIOR"; break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:	str << "UNDEFINED_BEHAVIOR"; break;
		case GL_DEBUG_TYPE_PORTABILITY:			str << "PORTABILITY"; break;
		case GL_DEBUG_TYPE_PERFORMANCE:			str << "PERFORMANCE"; break;
		case GL_DEBUG_TYPE_OTHER:				str << "OTHER"; break;
		default:								str << "UNKNOW"; break;
	}
	str << " " << id << ": " << message;
	switch (severity){
		case GL_DEBUG_SEVERITY_LOW:
			log->done(SSTR("LOW " << str.str()));
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			log->info(SSTR("MEDIUM " << str.str()));
			break;
		case GL_DEBUG_SEVERITY_HIGH:
			log->fail(SSTR("HIGH " << str.str()));
			break;
		default:
			log->init(SSTR("UNKNOW " << str.str()));
			break;
	}
}

openGlCtx::openGlCtx(int glCtxVMaj, int glCtxVMin) : cam(nullptr), log("GLFW") {
	glfwSetErrorCallback([](int error, const char* description) {
		//Log("GLFW").info(SSTR("Error " << error << ": " << description));
		std::cout << "[GLFW] Error " << error << ": " << description << std::endl;
	});

	setupThrow(log, glfwInit(),"Initialize");

	/* Setup OpenGL mode */ {
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, glCtxVMaj);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, glCtxVMin);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT , true);
		glfwWindowHint(GLFW_DOUBLEBUFFER , true);
		glfwWindowHint(GLFW_SAMPLES, 4);
	}

	/* Setup Video mode */ {
		auto monitor = glfwGetPrimaryMonitor();
		auto mode = glfwGetVideoMode(monitor);
		this->log.info(SSTR("Starting video in " << mode->width << "x" << mode->height));
        setupThrow(log, this->window = glfwCreateWindow(mode->width, mode->height, "TD", monitor, nullptr), "Creating FullScreen windows")/*
        setupThrow(log, this->window = glfwCreateWindow(960, 960, "TD", NULL, NULL), "Creating windows");//*/
	}

	/* Setup input mode */ {
//		glfwSetInputMode(*this, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetInputMode(*this, GLFW_STICKY_KEYS, true);
	}

	/* Setup Callbacks */ {
		glfwSetWindowUserPointer(*this, this);

		glfwSetFramebufferSizeCallback(*this, [](GLFWwindow *w, int width, int height) {
			glViewport(0, 0, width, height);

			static_cast<openGlCtx *>(glfwGetWindowUserPointer(w))->height = height;
			static_cast<openGlCtx *>(glfwGetWindowUserPointer(w))->width = width;
        });

		glfwSetScrollCallback(*this, [](GLFWwindow *w, double, double delta) {
			static_cast<openGlCtx *>(glfwGetWindowUserPointer(w))->fov -= delta;
		});

		glfwGetFramebufferSize(*this, &this->width, &this->height);
    }


	glfwMakeContextCurrent(this->window);
    glfwSwapInterval(1);


	/* Init GLEW */ {
		Log glew("GLEW");
		glewExperimental = true; // Nécessaire pour le profil core
		setupThrow(glew, glewInit() == GLEW_OK,"Initialize");

		glew.info("Using GLEW version : " + std::string(reinterpret_cast<const char*>(glewGetString(GLEW_VERSION))));
	}

	/* Setup debug context */ {
		openGLLog = new Log("OpenGL");
		setup((*openGLLog), glDebugMessageCallback, "Register OpenGL debug callback");

		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(openGLCallback, openGLLog);
	}


	/* GL func activation */ {
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_STENCIL_TEST);
		glEnable(GL_CULL_FACE);

		glEnable(GL_MULTISAMPLE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
}


openGlCtx::~openGlCtx() {
	delete openGLLog;
	log.done("Destroy window"); glfwDestroyWindow(this->window);
	log.done("Closed properly");glfwTerminate();
}

#include <utils/perfquery.h>
#include <glm/gtx/string_cast.hpp>
void openGlCtx::operator()() {
	PerfQuery pq;
	Fps f;

	glm::mat4 view = glm::mat4(1), projection;

	while(!glfwWindowShouldClose(*this) && glfwGetKey(*this, GLFW_KEY_ESCAPE) != GLFW_PRESS) {
		f.begin();pq.start();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		projection = glm::perspective(this->fov, (float)this->width / (float)this->height, 0.01f, 100.0f);

		/* Process camera */ {
			double x, y;
			glfwGetCursorPos(*this, &x, &y);

            if(cam != nullptr)
				view = cam->update(x, y);
		}

		this->group(projection * view);

		this->group(width, height);

		glfwSwapBuffers(*this);
		f.fpsEnd();pq.stop();
		std::cout << "POLY COUNT : " << pq.getValue() << std::endl;
		glfwPollEvents();
	}
}
